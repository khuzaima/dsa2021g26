﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ambulance_Service_System
{
    public partial class ucRegisterAmbulance : UserControl
    {

        private static ucRegisterAmbulance _instence;

        private int U_index;

        public static ucRegisterAmbulance Instence
        {
            get
            {
                if(_instence == null)
                {
                    _instence = new ucRegisterAmbulance();
                }
                return _instence;
            }
        }
        public ucRegisterAmbulance()
        {
            InitializeComponent();
            LoadDataInDtv();
        }

        private void LoadDataInDtv()
        {
            dtvAmbulances.Rows.Clear();
            string datestatus;
            for (int i = 0; i < cData.list_of_ambulances.Count; i++)
            {
                if (cData.list_of_ambulances[i].updateData_Date == DateTime.MinValue)
                {
                    datestatus = "Not Updated";
                }
                else
                {
                    datestatus = cData.list_of_ambulances[i].updateData_Date.ToString();
                }
                dtvAmbulances.Rows.Add(cData.list_of_ambulances[i].ID, cData.list_of_ambulances[i].plateNum, cData.list_of_ambulances[i].registration_Date.ToString(), datestatus);
            }
        }
        
        private String GenerateAmbulanceId()
        {
            String id = "";
            bool flag = true;
            for (int i = 1; i <= cData.list_of_ambulances.Count + 1; i++)
            {
                id = "AMB-" + i;
                for (int j = 0; j < cData.list_of_ambulances.Count; j++)
                {
                    if (id.Equals(cData.list_of_ambulances[j].ID))
                    {
                        flag = false;
                    }
                }
                if (flag == false)
                {
                    flag = true;
                }
                else
                {
                    return id;
                }
            }
            return id;
        }

        private bool ValidatePlateNum(String num)
        {
            bool flag = true;
            for (int j = 0; j < cData.list_of_ambulances.Count; j++)
            {
                if (num.Equals(cData.list_of_ambulances[j].plateNum))
                {
                    flag = false;
                }
            }
            return flag;
        }

        private void RegisterAmbulance()
        {
            if (ValidatePlateNum(tbAmbulancePlateNum.Text) == true)
            {
                cAmbulance ambulance = new cAmbulance(GenerateAmbulanceId(), tbAmbulancePlateNum.Text, DateTime.Now, DateTime.MinValue, "Unavilable");
                cData.list_of_ambulances.Add(ambulance);
                LoadDataInDtv();
                tbAmbulancePlateNum.Text = "";
            }
            else
            {
                MessageBox.Show("Invalid Plate Number.");
            }
        }

        private void UpdateAmbulance(int index)
        {
            if (ValidatePlateNum(tbAmbulancePlateNum.Text) == true)
            {
                cData.list_of_ambulances[index].plateNum = tbAmbulancePlateNum.Text;
                cData.list_of_ambulances[index].updateData_Date = DateTime.Now;
                LoadDataInDtv();
                MessageBox.Show("Data Updated Successfully.");
                btnRegister.Text = "Register";
                tbAmbulancePlateNum.Text = "";
            }
            else
            {
                MessageBox.Show("Invalid Plate Number.");
            }
        }

        private void btnRegister_Click(object sender, EventArgs e)
        {
            if (btnRegister.Text == "Register")
            {
                RegisterAmbulance();
            }
            else if (btnRegister.Text == "Update")
            {
                UpdateAmbulance(U_index);
            }
        }

        public void RefreshUC()
        {
            tbAmbulancePlateNum.Text = "";
            btnRegister.Text = "Register";
            LoadDataInDtv();
        }

        private void DeleteAmbulanceRow(int index)
        {
            cData.list_of_ambulances.RemoveAt(index);
            dtvAmbulances.Rows.RemoveAt(index);
        }

        private void dtvAmbulances_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                if (e.ColumnIndex == 5)
                {
                    DialogResult dr = MessageBox.Show("Are you sure you want to delete this doctor.", "Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dr == DialogResult.Yes)
                    {
                        DeleteAmbulanceRow(e.RowIndex);
                    }
                }
                if (e.ColumnIndex == 4)
                {
                    btnRegister.Text = "Update";
                    U_index = e.RowIndex;
                    tbAmbulancePlateNum.Text = cData.list_of_ambulances[e.RowIndex].plateNum;
                }
            }
        }
    }
}
