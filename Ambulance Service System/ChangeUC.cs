﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ambulance_Service_System
{
    class ChangeUC
    {
        #region SideMenuButtons

        public static void Home()
        {
            if (!MainWindow.main_Panel.Controls.Contains(ucHome.Instence))
            {
                MainWindow.main_Panel.Controls.Add(ucHome.Instence);
                ucHome.Instence.Dock = DockStyle.Fill;
                ucHome.Instence.BringToFront();
            }
            else
            {
                ucHome.Instence.BringToFront();
            }
        }

        public static void Patient_FC_Report()
        {
            if (!MainWindow.main_Panel.Controls.Contains(ucPatient_FC_Report.Instence))
            {
                MainWindow.main_Panel.Controls.Add(ucPatient_FC_Report.Instence);
                ucPatient_FC_Report.Instence.Dock = DockStyle.Fill;
                ucPatient_FC_Report.Instence.BringToFront();
            }
            else
            {
                ucPatient_FC_Report.Instence.RefreshUC();
                ucPatient_FC_Report.Instence.BringToFront();
            }
        }

        public static void StaffShifts()
        {
            if (!MainWindow.main_Panel.Controls.Contains(ucStaffShifts.Instence))
            {
                MainWindow.main_Panel.Controls.Add(ucStaffShifts.Instence);
                ucStaffShifts.Instence.Dock = DockStyle.Fill;
                ucStaffShifts.Instence.BringToFront();
            }
            else
            {
                ucStaffShifts.Instence.RefreshUC();
                ucStaffShifts.Instence.BringToFront();
            }
        }

        public static void AddStaff()
        {
            if (!MainWindow.main_Panel.Controls.Contains(ucAddStaff.Instence))
            {
                MainWindow.main_Panel.Controls.Add(ucAddStaff.Instence);
                ucAddStaff.Instence.Dock = DockStyle.Fill;
                ucAddStaff.Instence.BringToFront();
            }
            else
            {
                ucAddStaff.Instence.RefreshUC();
                ucAddStaff.Instence.BringToFront();
            }
        }

        public static void Hospitals()
        {
            if (!MainWindow.main_Panel.Controls.Contains(ucHospitals.Instence))
            {
                MainWindow.main_Panel.Controls.Add(ucHospitals.Instence);
                ucHospitals.Instence.Dock = DockStyle.Fill;
                ucHospitals.Instence.BringToFront();
            }
            else
            {
                ucHospitals.Instence.RefreshUC();
                ucHospitals.Instence.BringToFront();
            }
        }
        #endregion

        #region SideMenu->SubMenuButtons

        public static void DispatchAmbulace()
        {
            if (!MainWindow.main_Panel.Controls.Contains(ucDispatchAmbulance.Instence))
            {
                MainWindow.main_Panel.Controls.Add(ucDispatchAmbulance.Instence);
                ucDispatchAmbulance.Instence.Dock = DockStyle.Fill;
                ucDispatchAmbulance.Instence.BringToFront();
            }
            else
            {
                ucDispatchAmbulance.Instence.RefreshUC();
                ucDispatchAmbulance.Instence.BringToFront();
            }
        }

        public static void AmbulanceStatus()
        {
            if (!MainWindow.main_Panel.Controls.Contains(ucAmbulanceStatus.Instence))
            {
                MainWindow.main_Panel.Controls.Add(ucAmbulanceStatus.Instence);
                ucAmbulanceStatus.Instence.Dock = DockStyle.Fill;
                ucAmbulanceStatus.Instence.BringToFront();
            }
            else
            {
                ucAmbulanceStatus.Instence.RefreshUC();
                ucAmbulanceStatus.Instence.BringToFront();
            }
        }

        public static void RegisterAmbulance()
        {
            if (!MainWindow.main_Panel.Controls.Contains(ucRegisterAmbulance.Instence))
            {
                MainWindow.main_Panel.Controls.Add(ucRegisterAmbulance.Instence);
                ucRegisterAmbulance.Instence.Dock = DockStyle.Fill;
                ucRegisterAmbulance.Instence.BringToFront();
            }
            else
            {
                ucRegisterAmbulance.Instence.RefreshUC();
                ucRegisterAmbulance.Instence.BringToFront();
            }
        }

        public static void UpdateParking()
        {
            if (!MainWindow.main_Panel.Controls.Contains(ucUpdateParking.Instence))
            {
                MainWindow.main_Panel.Controls.Add(ucUpdateParking.Instence);
                ucUpdateParking.Instence.Dock = DockStyle.Fill;
                ucUpdateParking.Instence.BringToFront();
            }
            else
            {
                ucAmbulanceStatus.Instence.RefreshUC();
                ucUpdateParking.Instence.BringToFront();
            }
        }

        public static void ParkingLog()
        {
            if (!MainWindow.main_Panel.Controls.Contains(ucParkingLog.Instence))
            {
                MainWindow.main_Panel.Controls.Add(ucParkingLog.Instence);
                ucParkingLog.Instence.Dock = DockStyle.Fill;
                ucParkingLog.Instence.BringToFront();
            }
            else
            {
                ucParkingLog.Instence.RefreshUC();
                ucParkingLog.Instence.BringToFront();
            }
        }

        #endregion
    }
}
