﻿
namespace Ambulance_Service_System
{
    partial class ucRegisterAmbulance
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlHeader = new System.Windows.Forms.Panel();
            this.lbTitleAmbulances = new System.Windows.Forms.Label();
            this.pnlRegisterAmbulance = new System.Windows.Forms.Panel();
            this.tbAmbulancePlateNum = new System.Windows.Forms.TextBox();
            this.btnRegister = new System.Windows.Forms.Button();
            this.lbAmbulancePlateNum = new System.Windows.Forms.Label();
            this.lbRegisterNewAmbulance = new System.Windows.Forms.Label();
            this.dtvAmbulances = new System.Windows.Forms.DataGridView();
            this.lbAmbulances = new System.Windows.Forms.Label();
            this.pnlAmbulanceData = new System.Windows.Forms.Panel();
            this.AmbulanceID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AmbulancePlateNum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RegistrationDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UpdateDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UpdateAmb = new System.Windows.Forms.DataGridViewButtonColumn();
            this.DeleteAmb = new System.Windows.Forms.DataGridViewButtonColumn();
            this.pnlHeader.SuspendLayout();
            this.pnlRegisterAmbulance.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtvAmbulances)).BeginInit();
            this.pnlAmbulanceData.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlHeader
            // 
            this.pnlHeader.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pnlHeader.Controls.Add(this.lbTitleAmbulances);
            this.pnlHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlHeader.ForeColor = System.Drawing.SystemColors.ControlText;
            this.pnlHeader.Location = new System.Drawing.Point(0, 0);
            this.pnlHeader.Name = "pnlHeader";
            this.pnlHeader.Size = new System.Drawing.Size(837, 64);
            this.pnlHeader.TabIndex = 3;
            // 
            // lbTitleAmbulances
            // 
            this.lbTitleAmbulances.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lbTitleAmbulances.AutoSize = true;
            this.lbTitleAmbulances.BackColor = System.Drawing.Color.Transparent;
            this.lbTitleAmbulances.Font = new System.Drawing.Font("Imprint MT Shadow", 20F);
            this.lbTitleAmbulances.ForeColor = System.Drawing.Color.White;
            this.lbTitleAmbulances.Location = new System.Drawing.Point(315, 16);
            this.lbTitleAmbulances.Name = "lbTitleAmbulances";
            this.lbTitleAmbulances.Size = new System.Drawing.Size(217, 32);
            this.lbTitleAmbulances.TabIndex = 4;
            this.lbTitleAmbulances.Text = "AMBULANCES";
            // 
            // pnlRegisterAmbulance
            // 
            this.pnlRegisterAmbulance.Controls.Add(this.tbAmbulancePlateNum);
            this.pnlRegisterAmbulance.Controls.Add(this.btnRegister);
            this.pnlRegisterAmbulance.Controls.Add(this.lbAmbulancePlateNum);
            this.pnlRegisterAmbulance.Controls.Add(this.lbRegisterNewAmbulance);
            this.pnlRegisterAmbulance.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlRegisterAmbulance.Location = new System.Drawing.Point(0, 64);
            this.pnlRegisterAmbulance.Name = "pnlRegisterAmbulance";
            this.pnlRegisterAmbulance.Size = new System.Drawing.Size(837, 116);
            this.pnlRegisterAmbulance.TabIndex = 4;
            // 
            // tbAmbulancePlateNum
            // 
            this.tbAmbulancePlateNum.Location = new System.Drawing.Point(13, 68);
            this.tbAmbulancePlateNum.Name = "tbAmbulancePlateNum";
            this.tbAmbulancePlateNum.Size = new System.Drawing.Size(266, 20);
            this.tbAmbulancePlateNum.TabIndex = 26;
            // 
            // btnRegister
            // 
            this.btnRegister.BackColor = System.Drawing.SystemColors.ControlLight;
            this.btnRegister.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRegister.Location = new System.Drawing.Point(285, 62);
            this.btnRegister.Name = "btnRegister";
            this.btnRegister.Size = new System.Drawing.Size(102, 30);
            this.btnRegister.TabIndex = 25;
            this.btnRegister.Text = "Register";
            this.btnRegister.UseVisualStyleBackColor = false;
            this.btnRegister.Click += new System.EventHandler(this.btnRegister_Click);
            // 
            // lbAmbulancePlateNum
            // 
            this.lbAmbulancePlateNum.AutoSize = true;
            this.lbAmbulancePlateNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbAmbulancePlateNum.Location = new System.Drawing.Point(10, 48);
            this.lbAmbulancePlateNum.Name = "lbAmbulancePlateNum";
            this.lbAmbulancePlateNum.Size = new System.Drawing.Size(140, 17);
            this.lbAmbulancePlateNum.TabIndex = 6;
            this.lbAmbulancePlateNum.Text = "Ambulance Plate No.";
            // 
            // lbRegisterNewAmbulance
            // 
            this.lbRegisterNewAmbulance.AutoSize = true;
            this.lbRegisterNewAmbulance.BackColor = System.Drawing.Color.Transparent;
            this.lbRegisterNewAmbulance.Font = new System.Drawing.Font("Imprint MT Shadow", 14F);
            this.lbRegisterNewAmbulance.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lbRegisterNewAmbulance.Location = new System.Drawing.Point(4, 4);
            this.lbRegisterNewAmbulance.Name = "lbRegisterNewAmbulance";
            this.lbRegisterNewAmbulance.Size = new System.Drawing.Size(221, 23);
            this.lbRegisterNewAmbulance.TabIndex = 5;
            this.lbRegisterNewAmbulance.Text = "Register New Ambulance";
            // 
            // dtvAmbulances
            // 
            this.dtvAmbulances.AllowUserToAddRows = false;
            this.dtvAmbulances.AllowUserToDeleteRows = false;
            this.dtvAmbulances.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtvAmbulances.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dtvAmbulances.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtvAmbulances.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.AmbulanceID,
            this.AmbulancePlateNum,
            this.RegistrationDate,
            this.UpdateDate,
            this.UpdateAmb,
            this.DeleteAmb});
            this.dtvAmbulances.Location = new System.Drawing.Point(11, 29);
            this.dtvAmbulances.Name = "dtvAmbulances";
            this.dtvAmbulances.ReadOnly = true;
            this.dtvAmbulances.Size = new System.Drawing.Size(815, 300);
            this.dtvAmbulances.TabIndex = 9;
            this.dtvAmbulances.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtvAmbulances_CellContentClick);
            // 
            // lbAmbulances
            // 
            this.lbAmbulances.AutoSize = true;
            this.lbAmbulances.BackColor = System.Drawing.Color.Transparent;
            this.lbAmbulances.Font = new System.Drawing.Font("Imprint MT Shadow", 14F);
            this.lbAmbulances.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lbAmbulances.Location = new System.Drawing.Point(10, 3);
            this.lbAmbulances.Name = "lbAmbulances";
            this.lbAmbulances.Size = new System.Drawing.Size(112, 23);
            this.lbAmbulances.TabIndex = 10;
            this.lbAmbulances.Text = "Ambulances";
            // 
            // pnlAmbulanceData
            // 
            this.pnlAmbulanceData.Controls.Add(this.lbAmbulances);
            this.pnlAmbulanceData.Controls.Add(this.dtvAmbulances);
            this.pnlAmbulanceData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlAmbulanceData.Location = new System.Drawing.Point(0, 180);
            this.pnlAmbulanceData.Name = "pnlAmbulanceData";
            this.pnlAmbulanceData.Size = new System.Drawing.Size(837, 344);
            this.pnlAmbulanceData.TabIndex = 5;
            // 
            // AmbulanceID
            // 
            this.AmbulanceID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.AmbulanceID.HeaderText = "Ambulance ID";
            this.AmbulanceID.Name = "AmbulanceID";
            this.AmbulanceID.ReadOnly = true;
            // 
            // AmbulancePlateNum
            // 
            this.AmbulancePlateNum.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.AmbulancePlateNum.HeaderText = "Ambulance Plate No.";
            this.AmbulancePlateNum.Name = "AmbulancePlateNum";
            this.AmbulancePlateNum.ReadOnly = true;
            // 
            // RegistrationDate
            // 
            this.RegistrationDate.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.RegistrationDate.HeaderText = "Registeration Date";
            this.RegistrationDate.Name = "RegistrationDate";
            this.RegistrationDate.ReadOnly = true;
            // 
            // UpdateDate
            // 
            this.UpdateDate.HeaderText = "Updated";
            this.UpdateDate.Name = "UpdateDate";
            this.UpdateDate.ReadOnly = true;
            // 
            // UpdateAmb
            // 
            this.UpdateAmb.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.UpdateAmb.HeaderText = "Update";
            this.UpdateAmb.MinimumWidth = 50;
            this.UpdateAmb.Name = "UpdateAmb";
            this.UpdateAmb.ReadOnly = true;
            this.UpdateAmb.Text = "Update";
            this.UpdateAmb.UseColumnTextForButtonValue = true;
            this.UpdateAmb.Width = 50;
            // 
            // DeleteAmb
            // 
            this.DeleteAmb.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.DeleteAmb.HeaderText = "Delete";
            this.DeleteAmb.MinimumWidth = 50;
            this.DeleteAmb.Name = "DeleteAmb";
            this.DeleteAmb.ReadOnly = true;
            this.DeleteAmb.Text = "Delete";
            this.DeleteAmb.UseColumnTextForButtonValue = true;
            this.DeleteAmb.Width = 50;
            // 
            // ucRegisterAmbulance
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pnlAmbulanceData);
            this.Controls.Add(this.pnlRegisterAmbulance);
            this.Controls.Add(this.pnlHeader);
            this.Name = "ucRegisterAmbulance";
            this.Size = new System.Drawing.Size(837, 524);
            this.pnlHeader.ResumeLayout(false);
            this.pnlHeader.PerformLayout();
            this.pnlRegisterAmbulance.ResumeLayout(false);
            this.pnlRegisterAmbulance.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtvAmbulances)).EndInit();
            this.pnlAmbulanceData.ResumeLayout(false);
            this.pnlAmbulanceData.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlHeader;
        private System.Windows.Forms.Label lbTitleAmbulances;
        private System.Windows.Forms.Panel pnlRegisterAmbulance;
        private System.Windows.Forms.Button btnRegister;
        private System.Windows.Forms.Label lbAmbulancePlateNum;
        private System.Windows.Forms.Label lbRegisterNewAmbulance;
        private System.Windows.Forms.TextBox tbAmbulancePlateNum;
        private System.Windows.Forms.DataGridView dtvAmbulances;
        private System.Windows.Forms.Label lbAmbulances;
        private System.Windows.Forms.Panel pnlAmbulanceData;
        private System.Windows.Forms.DataGridViewTextBoxColumn AmbulanceID;
        private System.Windows.Forms.DataGridViewTextBoxColumn AmbulancePlateNum;
        private System.Windows.Forms.DataGridViewTextBoxColumn RegistrationDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn UpdateDate;
        private System.Windows.Forms.DataGridViewButtonColumn UpdateAmb;
        private System.Windows.Forms.DataGridViewButtonColumn DeleteAmb;
    }
}
