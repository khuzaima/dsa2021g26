﻿
namespace Ambulance_Service_System
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
            this.pnlHeaderGray = new System.Windows.Forms.Panel();
            this.lbASS_TitleTxt = new System.Windows.Forms.Label();
            this.pnlRightDBlue = new System.Windows.Forms.Panel();
            this.btnLogout = new System.Windows.Forms.Button();
            this.pnlRightDRed = new System.Windows.Forms.Panel();
            this.pnlSideMenu = new System.Windows.Forms.Panel();
            this.pnlSelectedBtn = new System.Windows.Forms.Panel();
            this.btnHospitals = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.pnlSideMenuBtmSpace = new System.Windows.Forms.Panel();
            this.btnAddStaff = new System.Windows.Forms.Button();
            this.btnShiftChange = new System.Windows.Forms.Button();
            this.pnlParkingSubMenu = new System.Windows.Forms.Panel();
            this.btnParkingLog = new System.Windows.Forms.Button();
            this.btnUpdateParking = new System.Windows.Forms.Button();
            this.btnRegisterdAmbulances = new System.Windows.Forms.Button();
            this.btnParkingManagement = new System.Windows.Forms.Button();
            this.btnPt_FC_Report = new System.Windows.Forms.Button();
            this.pnlDispatchSubMenu = new System.Windows.Forms.Panel();
            this.btnAmbulancesStatus = new System.Windows.Forms.Button();
            this.btnDispatch = new System.Windows.Forms.Button();
            this.btnDispatchAmbulances = new System.Windows.Forms.Button();
            this.btnHome = new System.Windows.Forms.Button();
            this.pnlSideMenuLogo = new System.Windows.Forms.Panel();
            this.panel = new System.Windows.Forms.Panel();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.logoutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pnlHeaderGray.SuspendLayout();
            this.pnlRightDBlue.SuspendLayout();
            this.pnlSideMenu.SuspendLayout();
            this.pnlParkingSubMenu.SuspendLayout();
            this.pnlDispatchSubMenu.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlHeaderGray
            // 
            this.pnlHeaderGray.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.pnlHeaderGray.Controls.Add(this.lbASS_TitleTxt);
            this.pnlHeaderGray.Controls.Add(this.pnlRightDBlue);
            this.pnlHeaderGray.Controls.Add(this.pnlRightDRed);
            this.pnlHeaderGray.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlHeaderGray.Location = new System.Drawing.Point(0, 0);
            this.pnlHeaderGray.Name = "pnlHeaderGray";
            this.pnlHeaderGray.Size = new System.Drawing.Size(1074, 77);
            this.pnlHeaderGray.TabIndex = 0;
            // 
            // lbASS_TitleTxt
            // 
            this.lbASS_TitleTxt.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lbASS_TitleTxt.AutoSize = true;
            this.lbASS_TitleTxt.BackColor = System.Drawing.Color.Transparent;
            this.lbASS_TitleTxt.Font = new System.Drawing.Font("Imprint MT Shadow", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbASS_TitleTxt.ForeColor = System.Drawing.Color.White;
            this.lbASS_TitleTxt.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.lbASS_TitleTxt.Location = new System.Drawing.Point(382, 22);
            this.lbASS_TitleTxt.Name = "lbASS_TitleTxt";
            this.lbASS_TitleTxt.Size = new System.Drawing.Size(293, 28);
            this.lbASS_TitleTxt.TabIndex = 17;
            this.lbASS_TitleTxt.Text = "Ambulance Service System";
            // 
            // pnlRightDBlue
            // 
            this.pnlRightDBlue.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(70)))), ((int)(((byte)(230)))));
            this.pnlRightDBlue.Controls.Add(this.btnLogout);
            this.pnlRightDBlue.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnlRightDBlue.Location = new System.Drawing.Point(540, 0);
            this.pnlRightDBlue.Name = "pnlRightDBlue";
            this.pnlRightDBlue.Size = new System.Drawing.Size(534, 77);
            this.pnlRightDBlue.TabIndex = 1;
            // 
            // btnLogout
            // 
            this.btnLogout.BackColor = System.Drawing.Color.Transparent;
            this.btnLogout.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnLogout.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(40)))), ((int)(((byte)(50)))));
            this.btnLogout.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.btnLogout.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLogout.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLogout.Image = ((System.Drawing.Image)(resources.GetObject("btnLogout.Image")));
            this.btnLogout.Location = new System.Drawing.Point(486, 22);
            this.btnLogout.Name = "btnLogout";
            this.btnLogout.Size = new System.Drawing.Size(37, 37);
            this.btnLogout.TabIndex = 0;
            this.btnLogout.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnLogout.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnLogout.UseVisualStyleBackColor = false;
            this.btnLogout.Click += new System.EventHandler(this.btnLogout_Click);
            // 
            // pnlRightDRed
            // 
            this.pnlRightDRed.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(40)))), ((int)(((byte)(50)))));
            this.pnlRightDRed.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlRightDRed.Location = new System.Drawing.Point(0, 0);
            this.pnlRightDRed.Name = "pnlRightDRed";
            this.pnlRightDRed.Size = new System.Drawing.Size(534, 77);
            this.pnlRightDRed.TabIndex = 0;
            // 
            // pnlSideMenu
            // 
            this.pnlSideMenu.AutoScroll = true;
            this.pnlSideMenu.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.pnlSideMenu.Controls.Add(this.pnlSelectedBtn);
            this.pnlSideMenu.Controls.Add(this.btnHospitals);
            this.pnlSideMenu.Controls.Add(this.btnExit);
            this.pnlSideMenu.Controls.Add(this.pnlSideMenuBtmSpace);
            this.pnlSideMenu.Controls.Add(this.btnAddStaff);
            this.pnlSideMenu.Controls.Add(this.btnShiftChange);
            this.pnlSideMenu.Controls.Add(this.pnlParkingSubMenu);
            this.pnlSideMenu.Controls.Add(this.btnParkingManagement);
            this.pnlSideMenu.Controls.Add(this.btnPt_FC_Report);
            this.pnlSideMenu.Controls.Add(this.pnlDispatchSubMenu);
            this.pnlSideMenu.Controls.Add(this.btnDispatchAmbulances);
            this.pnlSideMenu.Controls.Add(this.btnHome);
            this.pnlSideMenu.Controls.Add(this.pnlSideMenuLogo);
            this.pnlSideMenu.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlSideMenu.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.pnlSideMenu.Location = new System.Drawing.Point(0, 77);
            this.pnlSideMenu.Name = "pnlSideMenu";
            this.pnlSideMenu.Size = new System.Drawing.Size(237, 524);
            this.pnlSideMenu.TabIndex = 3;
            // 
            // pnlSelectedBtn
            // 
            this.pnlSelectedBtn.BackColor = System.Drawing.Color.White;
            this.pnlSelectedBtn.Location = new System.Drawing.Point(1, 143);
            this.pnlSelectedBtn.Name = "pnlSelectedBtn";
            this.pnlSelectedBtn.Size = new System.Drawing.Size(2, 45);
            this.pnlSelectedBtn.TabIndex = 0;
            // 
            // btnHospitals
            // 
            this.btnHospitals.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.btnHospitals.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnHospitals.FlatAppearance.BorderSize = 0;
            this.btnHospitals.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnHospitals.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.btnHospitals.ForeColor = System.Drawing.Color.White;
            this.btnHospitals.Image = ((System.Drawing.Image)(resources.GetObject("btnHospitals.Image")));
            this.btnHospitals.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnHospitals.Location = new System.Drawing.Point(0, 634);
            this.btnHospitals.Name = "btnHospitals";
            this.btnHospitals.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.btnHospitals.Size = new System.Drawing.Size(220, 45);
            this.btnHospitals.TabIndex = 21;
            this.btnHospitals.Text = " Hospitals";
            this.btnHospitals.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnHospitals.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnHospitals.UseVisualStyleBackColor = false;
            this.btnHospitals.Click += new System.EventHandler(this.btnHospitals_Click);
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.btnExit.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnExit.FlatAppearance.BorderSize = 0;
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExit.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.btnExit.ForeColor = System.Drawing.Color.White;
            this.btnExit.Image = ((System.Drawing.Image)(resources.GetObject("btnExit.Image")));
            this.btnExit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExit.Location = new System.Drawing.Point(0, 679);
            this.btnExit.Name = "btnExit";
            this.btnExit.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.btnExit.Size = new System.Drawing.Size(220, 45);
            this.btnExit.TabIndex = 20;
            this.btnExit.Text = " Exit";
            this.btnExit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExit.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // pnlSideMenuBtmSpace
            // 
            this.pnlSideMenuBtmSpace.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlSideMenuBtmSpace.Location = new System.Drawing.Point(0, 724);
            this.pnlSideMenuBtmSpace.Name = "pnlSideMenuBtmSpace";
            this.pnlSideMenuBtmSpace.Size = new System.Drawing.Size(220, 11);
            this.pnlSideMenuBtmSpace.TabIndex = 19;
            // 
            // btnAddStaff
            // 
            this.btnAddStaff.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.btnAddStaff.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnAddStaff.FlatAppearance.BorderSize = 0;
            this.btnAddStaff.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddStaff.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.btnAddStaff.ForeColor = System.Drawing.Color.White;
            this.btnAddStaff.Image = ((System.Drawing.Image)(resources.GetObject("btnAddStaff.Image")));
            this.btnAddStaff.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAddStaff.Location = new System.Drawing.Point(0, 589);
            this.btnAddStaff.Name = "btnAddStaff";
            this.btnAddStaff.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.btnAddStaff.Size = new System.Drawing.Size(220, 45);
            this.btnAddStaff.TabIndex = 17;
            this.btnAddStaff.Text = " Add Staff";
            this.btnAddStaff.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAddStaff.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnAddStaff.UseVisualStyleBackColor = false;
            this.btnAddStaff.Click += new System.EventHandler(this.btnAddStaff_Click);
            // 
            // btnShiftChange
            // 
            this.btnShiftChange.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.btnShiftChange.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnShiftChange.FlatAppearance.BorderSize = 0;
            this.btnShiftChange.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnShiftChange.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.btnShiftChange.ForeColor = System.Drawing.Color.White;
            this.btnShiftChange.Image = ((System.Drawing.Image)(resources.GetObject("btnShiftChange.Image")));
            this.btnShiftChange.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnShiftChange.Location = new System.Drawing.Point(0, 544);
            this.btnShiftChange.Name = "btnShiftChange";
            this.btnShiftChange.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.btnShiftChange.Size = new System.Drawing.Size(220, 45);
            this.btnShiftChange.TabIndex = 15;
            this.btnShiftChange.Text = " Shift Change";
            this.btnShiftChange.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnShiftChange.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnShiftChange.UseVisualStyleBackColor = false;
            this.btnShiftChange.Click += new System.EventHandler(this.btnShiftChange_Click);
            // 
            // pnlParkingSubMenu
            // 
            this.pnlParkingSubMenu.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pnlParkingSubMenu.Controls.Add(this.btnParkingLog);
            this.pnlParkingSubMenu.Controls.Add(this.btnUpdateParking);
            this.pnlParkingSubMenu.Controls.Add(this.btnRegisterdAmbulances);
            this.pnlParkingSubMenu.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlParkingSubMenu.Location = new System.Drawing.Point(0, 409);
            this.pnlParkingSubMenu.Name = "pnlParkingSubMenu";
            this.pnlParkingSubMenu.Size = new System.Drawing.Size(220, 135);
            this.pnlParkingSubMenu.TabIndex = 14;
            // 
            // btnParkingLog
            // 
            this.btnParkingLog.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btnParkingLog.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnParkingLog.FlatAppearance.BorderSize = 0;
            this.btnParkingLog.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnParkingLog.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.btnParkingLog.ForeColor = System.Drawing.Color.White;
            this.btnParkingLog.Image = ((System.Drawing.Image)(resources.GetObject("btnParkingLog.Image")));
            this.btnParkingLog.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnParkingLog.Location = new System.Drawing.Point(0, 90);
            this.btnParkingLog.Name = "btnParkingLog";
            this.btnParkingLog.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.btnParkingLog.Size = new System.Drawing.Size(220, 45);
            this.btnParkingLog.TabIndex = 4;
            this.btnParkingLog.Text = " Parking Log";
            this.btnParkingLog.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnParkingLog.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnParkingLog.UseVisualStyleBackColor = false;
            this.btnParkingLog.Click += new System.EventHandler(this.btnParkingLog_Click);
            // 
            // btnUpdateParking
            // 
            this.btnUpdateParking.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btnUpdateParking.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnUpdateParking.FlatAppearance.BorderSize = 0;
            this.btnUpdateParking.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUpdateParking.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.btnUpdateParking.ForeColor = System.Drawing.Color.White;
            this.btnUpdateParking.Image = ((System.Drawing.Image)(resources.GetObject("btnUpdateParking.Image")));
            this.btnUpdateParking.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnUpdateParking.Location = new System.Drawing.Point(0, 45);
            this.btnUpdateParking.Name = "btnUpdateParking";
            this.btnUpdateParking.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.btnUpdateParking.Size = new System.Drawing.Size(220, 45);
            this.btnUpdateParking.TabIndex = 3;
            this.btnUpdateParking.Text = " Update Parking";
            this.btnUpdateParking.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnUpdateParking.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnUpdateParking.UseVisualStyleBackColor = false;
            this.btnUpdateParking.Click += new System.EventHandler(this.btnUpdateParking_Click);
            // 
            // btnRegisterdAmbulances
            // 
            this.btnRegisterdAmbulances.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btnRegisterdAmbulances.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnRegisterdAmbulances.FlatAppearance.BorderSize = 0;
            this.btnRegisterdAmbulances.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRegisterdAmbulances.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.btnRegisterdAmbulances.ForeColor = System.Drawing.Color.White;
            this.btnRegisterdAmbulances.Image = ((System.Drawing.Image)(resources.GetObject("btnRegisterdAmbulances.Image")));
            this.btnRegisterdAmbulances.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRegisterdAmbulances.Location = new System.Drawing.Point(0, 0);
            this.btnRegisterdAmbulances.Name = "btnRegisterdAmbulances";
            this.btnRegisterdAmbulances.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.btnRegisterdAmbulances.Size = new System.Drawing.Size(220, 45);
            this.btnRegisterdAmbulances.TabIndex = 2;
            this.btnRegisterdAmbulances.Text = " Registerd Ambulances";
            this.btnRegisterdAmbulances.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRegisterdAmbulances.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnRegisterdAmbulances.UseVisualStyleBackColor = false;
            this.btnRegisterdAmbulances.Click += new System.EventHandler(this.btnRegisterdAmbulances_Click);
            // 
            // btnParkingManagement
            // 
            this.btnParkingManagement.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.btnParkingManagement.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnParkingManagement.FlatAppearance.BorderSize = 0;
            this.btnParkingManagement.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnParkingManagement.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.btnParkingManagement.ForeColor = System.Drawing.Color.White;
            this.btnParkingManagement.Image = ((System.Drawing.Image)(resources.GetObject("btnParkingManagement.Image")));
            this.btnParkingManagement.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnParkingManagement.Location = new System.Drawing.Point(0, 364);
            this.btnParkingManagement.Name = "btnParkingManagement";
            this.btnParkingManagement.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.btnParkingManagement.Size = new System.Drawing.Size(220, 45);
            this.btnParkingManagement.TabIndex = 13;
            this.btnParkingManagement.Text = " Parking Management";
            this.btnParkingManagement.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnParkingManagement.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnParkingManagement.UseVisualStyleBackColor = false;
            this.btnParkingManagement.Click += new System.EventHandler(this.btnParkingManagement_Click);
            // 
            // btnPt_FC_Report
            // 
            this.btnPt_FC_Report.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.btnPt_FC_Report.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnPt_FC_Report.FlatAppearance.BorderSize = 0;
            this.btnPt_FC_Report.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPt_FC_Report.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.btnPt_FC_Report.ForeColor = System.Drawing.Color.White;
            this.btnPt_FC_Report.Image = ((System.Drawing.Image)(resources.GetObject("btnPt_FC_Report.Image")));
            this.btnPt_FC_Report.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPt_FC_Report.Location = new System.Drawing.Point(0, 319);
            this.btnPt_FC_Report.Name = "btnPt_FC_Report";
            this.btnPt_FC_Report.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.btnPt_FC_Report.Size = new System.Drawing.Size(220, 45);
            this.btnPt_FC_Report.TabIndex = 12;
            this.btnPt_FC_Report.Text = " Patient FC Report";
            this.btnPt_FC_Report.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPt_FC_Report.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnPt_FC_Report.UseVisualStyleBackColor = false;
            this.btnPt_FC_Report.Click += new System.EventHandler(this.btnPt_FC_Report_Click);
            // 
            // pnlDispatchSubMenu
            // 
            this.pnlDispatchSubMenu.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pnlDispatchSubMenu.Controls.Add(this.btnAmbulancesStatus);
            this.pnlDispatchSubMenu.Controls.Add(this.btnDispatch);
            this.pnlDispatchSubMenu.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlDispatchSubMenu.Location = new System.Drawing.Point(0, 233);
            this.pnlDispatchSubMenu.Name = "pnlDispatchSubMenu";
            this.pnlDispatchSubMenu.Size = new System.Drawing.Size(220, 86);
            this.pnlDispatchSubMenu.TabIndex = 11;
            // 
            // btnAmbulancesStatus
            // 
            this.btnAmbulancesStatus.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btnAmbulancesStatus.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnAmbulancesStatus.FlatAppearance.BorderSize = 0;
            this.btnAmbulancesStatus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAmbulancesStatus.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.btnAmbulancesStatus.ForeColor = System.Drawing.Color.White;
            this.btnAmbulancesStatus.Image = ((System.Drawing.Image)(resources.GetObject("btnAmbulancesStatus.Image")));
            this.btnAmbulancesStatus.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAmbulancesStatus.Location = new System.Drawing.Point(0, 45);
            this.btnAmbulancesStatus.Name = "btnAmbulancesStatus";
            this.btnAmbulancesStatus.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.btnAmbulancesStatus.Size = new System.Drawing.Size(220, 45);
            this.btnAmbulancesStatus.TabIndex = 3;
            this.btnAmbulancesStatus.Text = " Ambulances Status";
            this.btnAmbulancesStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAmbulancesStatus.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnAmbulancesStatus.UseVisualStyleBackColor = false;
            this.btnAmbulancesStatus.Click += new System.EventHandler(this.btnAmbulancesStatus_Click);
            // 
            // btnDispatch
            // 
            this.btnDispatch.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btnDispatch.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnDispatch.FlatAppearance.BorderSize = 0;
            this.btnDispatch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDispatch.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.btnDispatch.ForeColor = System.Drawing.Color.White;
            this.btnDispatch.Image = ((System.Drawing.Image)(resources.GetObject("btnDispatch.Image")));
            this.btnDispatch.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDispatch.Location = new System.Drawing.Point(0, 0);
            this.btnDispatch.Name = "btnDispatch";
            this.btnDispatch.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.btnDispatch.Size = new System.Drawing.Size(220, 45);
            this.btnDispatch.TabIndex = 2;
            this.btnDispatch.Text = " Dispatch";
            this.btnDispatch.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDispatch.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnDispatch.UseVisualStyleBackColor = false;
            this.btnDispatch.Click += new System.EventHandler(this.btnDispatch_Click);
            // 
            // btnDispatchAmbulances
            // 
            this.btnDispatchAmbulances.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.btnDispatchAmbulances.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnDispatchAmbulances.FlatAppearance.BorderSize = 0;
            this.btnDispatchAmbulances.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDispatchAmbulances.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.btnDispatchAmbulances.ForeColor = System.Drawing.Color.White;
            this.btnDispatchAmbulances.Image = ((System.Drawing.Image)(resources.GetObject("btnDispatchAmbulances.Image")));
            this.btnDispatchAmbulances.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDispatchAmbulances.Location = new System.Drawing.Point(0, 188);
            this.btnDispatchAmbulances.Name = "btnDispatchAmbulances";
            this.btnDispatchAmbulances.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.btnDispatchAmbulances.Size = new System.Drawing.Size(220, 45);
            this.btnDispatchAmbulances.TabIndex = 10;
            this.btnDispatchAmbulances.Text = " Dispatch Ambulances";
            this.btnDispatchAmbulances.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDispatchAmbulances.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnDispatchAmbulances.UseVisualStyleBackColor = false;
            this.btnDispatchAmbulances.Click += new System.EventHandler(this.btnDispatchAmbulances_Click);
            // 
            // btnHome
            // 
            this.btnHome.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnHome.FlatAppearance.BorderSize = 0;
            this.btnHome.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnHome.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.btnHome.ForeColor = System.Drawing.Color.White;
            this.btnHome.Image = ((System.Drawing.Image)(resources.GetObject("btnHome.Image")));
            this.btnHome.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnHome.Location = new System.Drawing.Point(0, 143);
            this.btnHome.Name = "btnHome";
            this.btnHome.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.btnHome.Size = new System.Drawing.Size(220, 45);
            this.btnHome.TabIndex = 1;
            this.btnHome.Text = " Home";
            this.btnHome.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnHome.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnHome.UseVisualStyleBackColor = false;
            this.btnHome.Click += new System.EventHandler(this.btnHome_Click);
            // 
            // pnlSideMenuLogo
            // 
            this.pnlSideMenuLogo.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.pnlSideMenuLogo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pnlSideMenuLogo.BackgroundImage")));
            this.pnlSideMenuLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pnlSideMenuLogo.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlSideMenuLogo.Location = new System.Drawing.Point(0, 0);
            this.pnlSideMenuLogo.Name = "pnlSideMenuLogo";
            this.pnlSideMenuLogo.Size = new System.Drawing.Size(220, 143);
            this.pnlSideMenuLogo.TabIndex = 0;
            // 
            // panel
            // 
            this.panel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel.Location = new System.Drawing.Point(237, 77);
            this.panel.Name = "panel";
            this.panel.Size = new System.Drawing.Size(837, 524);
            this.panel.TabIndex = 5;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.logoutToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(113, 48);
            // 
            // logoutToolStripMenuItem
            // 
            this.logoutToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.logoutToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("logoutToolStripMenuItem.Image")));
            this.logoutToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.White;
            this.logoutToolStripMenuItem.Name = "logoutToolStripMenuItem";
            this.logoutToolStripMenuItem.Size = new System.Drawing.Size(112, 22);
            this.logoutToolStripMenuItem.Text = "Logout";
            this.logoutToolStripMenuItem.Click += new System.EventHandler(this.logoutToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.BackColor = System.Drawing.Color.White;
            this.exitToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("exitToolStripMenuItem.Image")));
            this.exitToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.White;
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(112, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1074, 601);
            this.Controls.Add(this.panel);
            this.Controls.Add(this.pnlSideMenu);
            this.Controls.Add(this.pnlHeaderGray);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(1090, 640);
            this.Name = "MainWindow";
            this.Text = "Ambulance Service System (ASS)";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainWindow_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainWindow_FormClosed);
            this.pnlHeaderGray.ResumeLayout(false);
            this.pnlHeaderGray.PerformLayout();
            this.pnlRightDBlue.ResumeLayout(false);
            this.pnlSideMenu.ResumeLayout(false);
            this.pnlParkingSubMenu.ResumeLayout(false);
            this.pnlDispatchSubMenu.ResumeLayout(false);
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlHeaderGray;
        private System.Windows.Forms.Panel pnlRightDBlue;
        private System.Windows.Forms.Panel pnlRightDRed;
        private System.Windows.Forms.Label lbASS_TitleTxt;
        private System.Windows.Forms.Button btnLogout;
        private System.Windows.Forms.Panel pnlSideMenu;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Panel pnlSideMenuBtmSpace;
        private System.Windows.Forms.Button btnAddStaff;
        private System.Windows.Forms.Button btnShiftChange;
        private System.Windows.Forms.Panel pnlParkingSubMenu;
        private System.Windows.Forms.Button btnParkingLog;
        private System.Windows.Forms.Button btnUpdateParking;
        private System.Windows.Forms.Button btnRegisterdAmbulances;
        private System.Windows.Forms.Button btnParkingManagement;
        private System.Windows.Forms.Button btnPt_FC_Report;
        private System.Windows.Forms.Panel pnlDispatchSubMenu;
        private System.Windows.Forms.Button btnAmbulancesStatus;
        private System.Windows.Forms.Button btnDispatch;
        private System.Windows.Forms.Button btnDispatchAmbulances;
        private System.Windows.Forms.Button btnHome;
        private System.Windows.Forms.Panel pnlSideMenuLogo;
        private System.Windows.Forms.Panel panel;
        private System.Windows.Forms.Button btnHospitals;
        private System.Windows.Forms.Panel pnlSelectedBtn;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem logoutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
    }
}