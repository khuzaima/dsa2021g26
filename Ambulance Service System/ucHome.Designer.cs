﻿
namespace Ambulance_Service_System
{
    partial class ucHome
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ucHome));
            this.pnlMainImage = new System.Windows.Forms.Panel();
            this.lbTitleHome = new System.Windows.Forms.Label();
            this.pbMainImage = new System.Windows.Forms.PictureBox();
            this.pnlVision = new System.Windows.Forms.Panel();
            this.rtbVision = new System.Windows.Forms.RichTextBox();
            this.lbVision = new System.Windows.Forms.Label();
            this.pnlMission = new System.Windows.Forms.Panel();
            this.rtbMission = new System.Windows.Forms.RichTextBox();
            this.lbMission = new System.Windows.Forms.Label();
            this.pnlObjectives = new System.Windows.Forms.Panel();
            this.rtbObjectives = new System.Windows.Forms.RichTextBox();
            this.lbObjectives = new System.Windows.Forms.Label();
            this.pnlOath = new System.Windows.Forms.Panel();
            this.rtbOath = new System.Windows.Forms.RichTextBox();
            this.lbOath = new System.Windows.Forms.Label();
            this.pnlMainImage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbMainImage)).BeginInit();
            this.pnlVision.SuspendLayout();
            this.pnlMission.SuspendLayout();
            this.pnlObjectives.SuspendLayout();
            this.pnlOath.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlMainImage
            // 
            this.pnlMainImage.BackColor = System.Drawing.Color.Transparent;
            this.pnlMainImage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlMainImage.Controls.Add(this.lbTitleHome);
            this.pnlMainImage.Controls.Add(this.pbMainImage);
            this.pnlMainImage.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlMainImage.Location = new System.Drawing.Point(0, 0);
            this.pnlMainImage.Name = "pnlMainImage";
            this.pnlMainImage.Size = new System.Drawing.Size(786, 739);
            this.pnlMainImage.TabIndex = 0;
            // 
            // lbTitleHome
            // 
            this.lbTitleHome.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lbTitleHome.AutoSize = true;
            this.lbTitleHome.BackColor = System.Drawing.Color.Black;
            this.lbTitleHome.Font = new System.Drawing.Font("Imprint MT Shadow", 20F);
            this.lbTitleHome.ForeColor = System.Drawing.Color.White;
            this.lbTitleHome.Location = new System.Drawing.Point(367, 46);
            this.lbTitleHome.Name = "lbTitleHome";
            this.lbTitleHome.Size = new System.Drawing.Size(104, 32);
            this.lbTitleHome.TabIndex = 2;
            this.lbTitleHome.Text = "HOME";
            // 
            // pbMainImage
            // 
            this.pbMainImage.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pbMainImage.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbMainImage.BackgroundImage")));
            this.pbMainImage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pbMainImage.Location = new System.Drawing.Point(0, 0);
            this.pbMainImage.Name = "pbMainImage";
            this.pbMainImage.Size = new System.Drawing.Size(786, 736);
            this.pbMainImage.TabIndex = 1;
            this.pbMainImage.TabStop = false;
            // 
            // pnlVision
            // 
            this.pnlVision.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlVision.Controls.Add(this.rtbVision);
            this.pnlVision.Controls.Add(this.lbVision);
            this.pnlVision.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlVision.Location = new System.Drawing.Point(0, 739);
            this.pnlVision.Name = "pnlVision";
            this.pnlVision.Size = new System.Drawing.Size(786, 95);
            this.pnlVision.TabIndex = 1;
            // 
            // rtbVision
            // 
            this.rtbVision.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rtbVision.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtbVision.Enabled = false;
            this.rtbVision.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtbVision.Location = new System.Drawing.Point(18, 36);
            this.rtbVision.Name = "rtbVision";
            this.rtbVision.Size = new System.Drawing.Size(733, 40);
            this.rtbVision.TabIndex = 2;
            this.rtbVision.Text = "Safe communities where all citizens are provided the right to timely emergency re" +
    "sponse and care without discrimination.";
            // 
            // lbVision
            // 
            this.lbVision.AutoSize = true;
            this.lbVision.Font = new System.Drawing.Font("Imprint MT Shadow", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVision.Location = new System.Drawing.Point(14, 12);
            this.lbVision.Name = "lbVision";
            this.lbVision.Size = new System.Drawing.Size(59, 21);
            this.lbVision.TabIndex = 1;
            this.lbVision.Text = "Vision";
            // 
            // pnlMission
            // 
            this.pnlMission.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlMission.Controls.Add(this.rtbMission);
            this.pnlMission.Controls.Add(this.lbMission);
            this.pnlMission.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlMission.Location = new System.Drawing.Point(0, 834);
            this.pnlMission.Name = "pnlMission";
            this.pnlMission.Size = new System.Drawing.Size(786, 110);
            this.pnlMission.TabIndex = 2;
            // 
            // rtbMission
            // 
            this.rtbMission.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rtbMission.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtbMission.Enabled = false;
            this.rtbMission.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtbMission.Location = new System.Drawing.Point(18, 41);
            this.rtbMission.Name = "rtbMission";
            this.rtbMission.Size = new System.Drawing.Size(733, 49);
            this.rtbMission.TabIndex = 3;
            this.rtbMission.Text = "Establishment of an effective system for emergency preparedness, response, protec" +
    "tion & prevention; while contributing towards building socially responsible, hea" +
    "lthy, resilient and safer communities.";
            // 
            // lbMission
            // 
            this.lbMission.AutoSize = true;
            this.lbMission.Font = new System.Drawing.Font("Imprint MT Shadow", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbMission.Location = new System.Drawing.Point(14, 17);
            this.lbMission.Name = "lbMission";
            this.lbMission.Size = new System.Drawing.Size(70, 21);
            this.lbMission.TabIndex = 2;
            this.lbMission.Text = "Mission";
            // 
            // pnlObjectives
            // 
            this.pnlObjectives.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlObjectives.Controls.Add(this.rtbObjectives);
            this.pnlObjectives.Controls.Add(this.lbObjectives);
            this.pnlObjectives.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlObjectives.Location = new System.Drawing.Point(0, 944);
            this.pnlObjectives.Name = "pnlObjectives";
            this.pnlObjectives.Size = new System.Drawing.Size(786, 200);
            this.pnlObjectives.TabIndex = 3;
            // 
            // rtbObjectives
            // 
            this.rtbObjectives.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rtbObjectives.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtbObjectives.Enabled = false;
            this.rtbObjectives.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtbObjectives.Location = new System.Drawing.Point(18, 41);
            this.rtbObjectives.Name = "rtbObjectives";
            this.rtbObjectives.Size = new System.Drawing.Size(733, 151);
            this.rtbObjectives.TabIndex = 3;
            this.rtbObjectives.Text = resources.GetString("rtbObjectives.Text");
            // 
            // lbObjectives
            // 
            this.lbObjectives.AutoSize = true;
            this.lbObjectives.Font = new System.Drawing.Font("Imprint MT Shadow", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbObjectives.Location = new System.Drawing.Point(14, 17);
            this.lbObjectives.Name = "lbObjectives";
            this.lbObjectives.Size = new System.Drawing.Size(127, 21);
            this.lbObjectives.TabIndex = 2;
            this.lbObjectives.Text = "Key Objectives";
            // 
            // pnlOath
            // 
            this.pnlOath.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlOath.Controls.Add(this.rtbOath);
            this.pnlOath.Controls.Add(this.lbOath);
            this.pnlOath.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlOath.Location = new System.Drawing.Point(0, 1144);
            this.pnlOath.Name = "pnlOath";
            this.pnlOath.Size = new System.Drawing.Size(786, 312);
            this.pnlOath.TabIndex = 4;
            // 
            // rtbOath
            // 
            this.rtbOath.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rtbOath.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtbOath.Enabled = false;
            this.rtbOath.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtbOath.Location = new System.Drawing.Point(18, 41);
            this.rtbOath.Name = "rtbOath";
            this.rtbOath.Size = new System.Drawing.Size(733, 249);
            this.rtbOath.TabIndex = 3;
            this.rtbOath.Text = resources.GetString("rtbOath.Text");
            // 
            // lbOath
            // 
            this.lbOath.AutoSize = true;
            this.lbOath.Font = new System.Drawing.Font("Imprint MT Shadow", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbOath.Location = new System.Drawing.Point(14, 17);
            this.lbOath.Name = "lbOath";
            this.lbOath.Size = new System.Drawing.Size(127, 21);
            this.lbOath.TabIndex = 2;
            this.lbOath.Text = "Rescuer\'s Oath";
            // 
            // ucHome
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.Controls.Add(this.pnlOath);
            this.Controls.Add(this.pnlObjectives);
            this.Controls.Add(this.pnlMission);
            this.Controls.Add(this.pnlVision);
            this.Controls.Add(this.pnlMainImage);
            this.Name = "ucHome";
            this.Size = new System.Drawing.Size(786, 524);
            this.pnlMainImage.ResumeLayout(false);
            this.pnlMainImage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbMainImage)).EndInit();
            this.pnlVision.ResumeLayout(false);
            this.pnlVision.PerformLayout();
            this.pnlMission.ResumeLayout(false);
            this.pnlMission.PerformLayout();
            this.pnlObjectives.ResumeLayout(false);
            this.pnlObjectives.PerformLayout();
            this.pnlOath.ResumeLayout(false);
            this.pnlOath.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlMainImage;
        private System.Windows.Forms.Panel pnlVision;
        private System.Windows.Forms.RichTextBox rtbVision;
        private System.Windows.Forms.Label lbVision;
        private System.Windows.Forms.Panel pnlMission;
        private System.Windows.Forms.Label lbMission;
        private System.Windows.Forms.Panel pnlObjectives;
        private System.Windows.Forms.RichTextBox rtbObjectives;
        private System.Windows.Forms.Label lbObjectives;
        private System.Windows.Forms.RichTextBox rtbMission;
        private System.Windows.Forms.Panel pnlOath;
        private System.Windows.Forms.Label lbOath;
        private System.Windows.Forms.RichTextBox rtbOath;
        private System.Windows.Forms.PictureBox pbMainImage;
        private System.Windows.Forms.Label lbTitleHome;
    }
}
