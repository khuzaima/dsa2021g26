﻿
namespace Ambulance_Service_System
{
    partial class ucHospitals
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlHeader = new System.Windows.Forms.Panel();
            this.lbTitleHospitals = new System.Windows.Forms.Label();
            this.pnlBody = new System.Windows.Forms.Panel();
            this.lbSearch = new System.Windows.Forms.Label();
            this.tbSearch = new System.Windows.Forms.TextBox();
            this.lbFCReports = new System.Windows.Forms.Label();
            this.dtvFC_Reports = new System.Windows.Forms.DataGridView();
            this.PatientID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EvacuationHospital = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.View = new System.Windows.Forms.DataGridViewButtonColumn();
            this.Reject = new System.Windows.Forms.DataGridViewButtonColumn();
            this.Accept = new System.Windows.Forms.DataGridViewButtonColumn();
            this.pnlHeader.SuspendLayout();
            this.pnlBody.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtvFC_Reports)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlHeader
            // 
            this.pnlHeader.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pnlHeader.Controls.Add(this.lbTitleHospitals);
            this.pnlHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlHeader.ForeColor = System.Drawing.SystemColors.ControlText;
            this.pnlHeader.Location = new System.Drawing.Point(0, 0);
            this.pnlHeader.Name = "pnlHeader";
            this.pnlHeader.Size = new System.Drawing.Size(837, 64);
            this.pnlHeader.TabIndex = 4;
            // 
            // lbTitleHospitals
            // 
            this.lbTitleHospitals.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lbTitleHospitals.AutoSize = true;
            this.lbTitleHospitals.BackColor = System.Drawing.Color.Transparent;
            this.lbTitleHospitals.Font = new System.Drawing.Font("Imprint MT Shadow", 20F);
            this.lbTitleHospitals.ForeColor = System.Drawing.Color.White;
            this.lbTitleHospitals.Location = new System.Drawing.Point(335, 16);
            this.lbTitleHospitals.Name = "lbTitleHospitals";
            this.lbTitleHospitals.Size = new System.Drawing.Size(181, 32);
            this.lbTitleHospitals.TabIndex = 4;
            this.lbTitleHospitals.Text = "HOSPITALS";
            // 
            // pnlBody
            // 
            this.pnlBody.Controls.Add(this.lbSearch);
            this.pnlBody.Controls.Add(this.tbSearch);
            this.pnlBody.Controls.Add(this.lbFCReports);
            this.pnlBody.Controls.Add(this.dtvFC_Reports);
            this.pnlBody.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlBody.Location = new System.Drawing.Point(0, 64);
            this.pnlBody.Name = "pnlBody";
            this.pnlBody.Size = new System.Drawing.Size(837, 460);
            this.pnlBody.TabIndex = 7;
            // 
            // lbSearch
            // 
            this.lbSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbSearch.AutoSize = true;
            this.lbSearch.Location = new System.Drawing.Point(629, 27);
            this.lbSearch.Name = "lbSearch";
            this.lbSearch.Size = new System.Drawing.Size(41, 13);
            this.lbSearch.TabIndex = 14;
            this.lbSearch.Text = "Search";
            // 
            // tbSearch
            // 
            this.tbSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tbSearch.Location = new System.Drawing.Point(672, 24);
            this.tbSearch.Name = "tbSearch";
            this.tbSearch.Size = new System.Drawing.Size(154, 20);
            this.tbSearch.TabIndex = 13;
            this.tbSearch.TextChanged += new System.EventHandler(this.tbSearch_TextChanged);
            // 
            // lbFCReports
            // 
            this.lbFCReports.AutoSize = true;
            this.lbFCReports.BackColor = System.Drawing.Color.Transparent;
            this.lbFCReports.Font = new System.Drawing.Font("Imprint MT Shadow", 14F);
            this.lbFCReports.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lbFCReports.Location = new System.Drawing.Point(9, 21);
            this.lbFCReports.Name = "lbFCReports";
            this.lbFCReports.Size = new System.Drawing.Size(251, 23);
            this.lbFCReports.TabIndex = 10;
            this.lbFCReports.Text = "Patient First Contact Reports";
            // 
            // dtvFC_Reports
            // 
            this.dtvFC_Reports.AllowUserToAddRows = false;
            this.dtvFC_Reports.AllowUserToDeleteRows = false;
            this.dtvFC_Reports.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtvFC_Reports.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dtvFC_Reports.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtvFC_Reports.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PatientID,
            this.EvacuationHospital,
            this.View,
            this.Reject,
            this.Accept});
            this.dtvFC_Reports.Location = new System.Drawing.Point(11, 47);
            this.dtvFC_Reports.Name = "dtvFC_Reports";
            this.dtvFC_Reports.ReadOnly = true;
            this.dtvFC_Reports.Size = new System.Drawing.Size(815, 398);
            this.dtvFC_Reports.TabIndex = 9;
            this.dtvFC_Reports.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtvFC_Reports_CellContentClick);
            // 
            // PatientID
            // 
            this.PatientID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.PatientID.HeaderText = "Patient ID ";
            this.PatientID.Name = "PatientID";
            this.PatientID.ReadOnly = true;
            // 
            // EvacuationHospital
            // 
            this.EvacuationHospital.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.EvacuationHospital.HeaderText = "Evacuation Hospital";
            this.EvacuationHospital.Name = "EvacuationHospital";
            this.EvacuationHospital.ReadOnly = true;
            // 
            // View
            // 
            this.View.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.View.HeaderText = "View";
            this.View.MinimumWidth = 50;
            this.View.Name = "View";
            this.View.ReadOnly = true;
            this.View.Text = "View";
            this.View.UseColumnTextForButtonValue = true;
            this.View.Width = 50;
            // 
            // Reject
            // 
            this.Reject.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Reject.HeaderText = "Reject";
            this.Reject.MinimumWidth = 50;
            this.Reject.Name = "Reject";
            this.Reject.ReadOnly = true;
            this.Reject.Text = "Reject";
            this.Reject.UseColumnTextForButtonValue = true;
            this.Reject.Width = 50;
            // 
            // Accept
            // 
            this.Accept.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Accept.HeaderText = "Accept";
            this.Accept.MinimumWidth = 50;
            this.Accept.Name = "Accept";
            this.Accept.ReadOnly = true;
            this.Accept.Text = "Accept";
            this.Accept.UseColumnTextForButtonValue = true;
            this.Accept.Width = 50;
            // 
            // ucHospitals
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pnlBody);
            this.Controls.Add(this.pnlHeader);
            this.Name = "ucHospitals";
            this.Size = new System.Drawing.Size(837, 524);
            this.pnlHeader.ResumeLayout(false);
            this.pnlHeader.PerformLayout();
            this.pnlBody.ResumeLayout(false);
            this.pnlBody.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtvFC_Reports)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlHeader;
        private System.Windows.Forms.Label lbTitleHospitals;
        private System.Windows.Forms.Panel pnlBody;
        private System.Windows.Forms.Label lbFCReports;
        private System.Windows.Forms.DataGridView dtvFC_Reports;
        private System.Windows.Forms.Label lbSearch;
        private System.Windows.Forms.TextBox tbSearch;
        private System.Windows.Forms.DataGridViewTextBoxColumn PatientID;
        private System.Windows.Forms.DataGridViewTextBoxColumn EvacuationHospital;
        private System.Windows.Forms.DataGridViewButtonColumn View;
        private System.Windows.Forms.DataGridViewButtonColumn Reject;
        private System.Windows.Forms.DataGridViewButtonColumn Accept;
    }
}
