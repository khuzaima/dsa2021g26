﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ambulance_Service_System
{
    class cFC_Report
    {
        public string id { get; set; }
        public string name { get; set; }
        public double height { get; set; }
        public int age { get; set; }
        public string treatmentLocation { get; set; }
        public string hospital { get; set; }
        public string injury { get; set; }

        public cFC_Report()
        {
            id = "";
            name = "";
            height = 0.0;
            age = 0;
            treatmentLocation = "";
            hospital = "";
            injury = "";
        }

        public cFC_Report(string i, string nam, double heigh, int ag, string location, string hosp, string inj)
        {
            id = i;
            name = nam;
            height = heigh;
            age = ag;
            treatmentLocation = location;
            hospital = hosp;
            injury = inj;
        }
    }
}
