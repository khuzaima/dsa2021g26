﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ambulance_Service_System
{
    class cParkingReport
    {
        public string ambID { get; set; }
        public string ambPlateNum { get; set; }
        public DateTime dateTime { get; set; }
        public string status { get; set; }

        public cParkingReport()
        {
            ambID = "";
            ambPlateNum = "";
            dateTime = DateTime.MinValue;
            status = "";
        }

        public cParkingReport(string id, string num, DateTime time, string stat)
        {
            ambID = id;
            ambPlateNum = num;
            dateTime = time;
            status = stat;
        }
    }
}
