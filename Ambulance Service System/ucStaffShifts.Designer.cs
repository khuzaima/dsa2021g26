﻿
namespace Ambulance_Service_System
{
    partial class ucStaffShifts
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlHeader = new System.Windows.Forms.Panel();
            this.lbTitleStaffShift = new System.Windows.Forms.Label();
            this.pnlBody = new System.Windows.Forms.Panel();
            this.cbChangeShift = new System.Windows.Forms.ComboBox();
            this.lbChangeShift = new System.Windows.Forms.Label();
            this.lbSearch = new System.Windows.Forms.Label();
            this.tbSearch = new System.Windows.Forms.TextBox();
            this.lbStaff = new System.Windows.Forms.Label();
            this.dtvShiftStaff = new System.Windows.Forms.DataGridView();
            this.StaffID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StaffName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.View = new System.Windows.Forms.DataGridViewButtonColumn();
            this.pnlHeader.SuspendLayout();
            this.pnlBody.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtvShiftStaff)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlHeader
            // 
            this.pnlHeader.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pnlHeader.Controls.Add(this.lbTitleStaffShift);
            this.pnlHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlHeader.ForeColor = System.Drawing.SystemColors.ControlText;
            this.pnlHeader.Location = new System.Drawing.Point(0, 0);
            this.pnlHeader.Name = "pnlHeader";
            this.pnlHeader.Size = new System.Drawing.Size(837, 64);
            this.pnlHeader.TabIndex = 8;
            // 
            // lbTitleStaffShift
            // 
            this.lbTitleStaffShift.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lbTitleStaffShift.AutoSize = true;
            this.lbTitleStaffShift.BackColor = System.Drawing.Color.Transparent;
            this.lbTitleStaffShift.Font = new System.Drawing.Font("Imprint MT Shadow", 20F);
            this.lbTitleStaffShift.ForeColor = System.Drawing.Color.White;
            this.lbTitleStaffShift.Location = new System.Drawing.Point(301, 16);
            this.lbTitleStaffShift.Name = "lbTitleStaffShift";
            this.lbTitleStaffShift.Size = new System.Drawing.Size(218, 32);
            this.lbTitleStaffShift.TabIndex = 4;
            this.lbTitleStaffShift.Text = "STAFF SHIFTS";
            // 
            // pnlBody
            // 
            this.pnlBody.Controls.Add(this.cbChangeShift);
            this.pnlBody.Controls.Add(this.lbChangeShift);
            this.pnlBody.Controls.Add(this.lbSearch);
            this.pnlBody.Controls.Add(this.tbSearch);
            this.pnlBody.Controls.Add(this.lbStaff);
            this.pnlBody.Controls.Add(this.dtvShiftStaff);
            this.pnlBody.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlBody.Location = new System.Drawing.Point(0, 64);
            this.pnlBody.Name = "pnlBody";
            this.pnlBody.Size = new System.Drawing.Size(837, 460);
            this.pnlBody.TabIndex = 10;
            // 
            // cbChangeShift
            // 
            this.cbChangeShift.FormattingEnabled = true;
            this.cbChangeShift.Items.AddRange(new object[] {
            "1st Shift",
            "2nd Shift",
            "3rd Shift"});
            this.cbChangeShift.Location = new System.Drawing.Point(97, 21);
            this.cbChangeShift.Name = "cbChangeShift";
            this.cbChangeShift.Size = new System.Drawing.Size(121, 21);
            this.cbChangeShift.TabIndex = 20;
            this.cbChangeShift.SelectedIndexChanged += new System.EventHandler(this.cbChangeShift_SelectedIndexChanged);
            // 
            // lbChangeShift
            // 
            this.lbChangeShift.AutoSize = true;
            this.lbChangeShift.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbChangeShift.Location = new System.Drawing.Point(10, 22);
            this.lbChangeShift.Name = "lbChangeShift";
            this.lbChangeShift.Size = new System.Drawing.Size(81, 17);
            this.lbChangeShift.TabIndex = 19;
            this.lbChangeShift.Text = "Change Shift";
            // 
            // lbSearch
            // 
            this.lbSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbSearch.AutoSize = true;
            this.lbSearch.Location = new System.Drawing.Point(629, 61);
            this.lbSearch.Name = "lbSearch";
            this.lbSearch.Size = new System.Drawing.Size(41, 13);
            this.lbSearch.TabIndex = 18;
            this.lbSearch.Text = "Search";
            // 
            // tbSearch
            // 
            this.tbSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tbSearch.Location = new System.Drawing.Point(672, 58);
            this.tbSearch.Name = "tbSearch";
            this.tbSearch.Size = new System.Drawing.Size(154, 20);
            this.tbSearch.TabIndex = 17;
            this.tbSearch.TextChanged += new System.EventHandler(this.tbSearch_TextChanged);
            // 
            // lbStaff
            // 
            this.lbStaff.AutoSize = true;
            this.lbStaff.BackColor = System.Drawing.Color.Transparent;
            this.lbStaff.Font = new System.Drawing.Font("Imprint MT Shadow", 14F);
            this.lbStaff.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lbStaff.Location = new System.Drawing.Point(9, 58);
            this.lbStaff.Name = "lbStaff";
            this.lbStaff.Size = new System.Drawing.Size(48, 23);
            this.lbStaff.TabIndex = 16;
            this.lbStaff.Text = "Staff";
            // 
            // dtvShiftStaff
            // 
            this.dtvShiftStaff.AllowUserToAddRows = false;
            this.dtvShiftStaff.AllowUserToDeleteRows = false;
            this.dtvShiftStaff.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtvShiftStaff.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dtvShiftStaff.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtvShiftStaff.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.StaffID,
            this.StaffName,
            this.View});
            this.dtvShiftStaff.Location = new System.Drawing.Point(13, 84);
            this.dtvShiftStaff.Name = "dtvShiftStaff";
            this.dtvShiftStaff.ReadOnly = true;
            this.dtvShiftStaff.Size = new System.Drawing.Size(815, 355);
            this.dtvShiftStaff.TabIndex = 15;
            this.dtvShiftStaff.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtvShiftStaff_CellContentClick);
            // 
            // StaffID
            // 
            this.StaffID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.StaffID.HeaderText = "Staff ID";
            this.StaffID.Name = "StaffID";
            this.StaffID.ReadOnly = true;
            // 
            // StaffName
            // 
            this.StaffName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.StaffName.HeaderText = "Name";
            this.StaffName.Name = "StaffName";
            this.StaffName.ReadOnly = true;
            // 
            // View
            // 
            this.View.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.View.HeaderText = "View";
            this.View.MinimumWidth = 50;
            this.View.Name = "View";
            this.View.ReadOnly = true;
            this.View.Text = "View";
            this.View.UseColumnTextForButtonValue = true;
            this.View.Width = 50;
            // 
            // ucStaffShifts
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pnlBody);
            this.Controls.Add(this.pnlHeader);
            this.Name = "ucStaffShifts";
            this.Size = new System.Drawing.Size(837, 524);
            this.pnlHeader.ResumeLayout(false);
            this.pnlHeader.PerformLayout();
            this.pnlBody.ResumeLayout(false);
            this.pnlBody.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtvShiftStaff)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel pnlHeader;
        private System.Windows.Forms.Label lbTitleStaffShift;
        private System.Windows.Forms.Panel pnlBody;
        private System.Windows.Forms.ComboBox cbChangeShift;
        private System.Windows.Forms.Label lbChangeShift;
        private System.Windows.Forms.Label lbSearch;
        private System.Windows.Forms.TextBox tbSearch;
        private System.Windows.Forms.Label lbStaff;
        private System.Windows.Forms.DataGridView dtvShiftStaff;
        private System.Windows.Forms.DataGridViewTextBoxColumn StaffID;
        private System.Windows.Forms.DataGridViewTextBoxColumn StaffName;
        private System.Windows.Forms.DataGridViewButtonColumn View;
    }
}
