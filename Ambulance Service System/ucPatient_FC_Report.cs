﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ambulance_Service_System
{
    public partial class ucPatient_FC_Report : UserControl
    {

        private static ucPatient_FC_Report _instence;
        public static ucPatient_FC_Report Instence
        {
            get
            {
                if (_instence == null)
                {
                    _instence = new ucPatient_FC_Report();
                }
                return _instence;
            }
        }
        public ucPatient_FC_Report()
        {
            InitializeComponent();
        }

        private void clearFields()
        {
            tbAge.Text = "";
            tbHeight.Text = "";
            tbName.Text = "";
            rtbInjury.Text = "";
            cbTreatmentLocation.SelectedItem = null;
            cbEvacuatedTo.SelectedItem = null;
        }

        public void RefreshUC()
        {
            clearFields();
        }

        private String GenerateReportId()
        {
            String id = "";
            bool flag = true;
            for (int i = 1; i <= cData.list_of_FC_reports.Count + 1; i++)
            {
                id = "RPT-" + i;
                for (int j = 0; j < cData.list_of_FC_reports.Count; j++)
                {
                    if (id.Equals(cData.list_of_FC_reports[j].id))
                    {
                        flag = false;
                    }
                }
                if (flag == false)
                {
                    flag = true;
                }
                else
                {
                    return id;
                }
            }
            return id;
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            if (tbAge.Text != "" && tbHeight.Text != "" && tbName.Text != "" && rtbInjury.Text != "" && cbTreatmentLocation.SelectedItem != null && cbEvacuatedTo.SelectedItem != null)
            {
                cData.list_of_FC_reports.Add(new cFC_Report(GenerateReportId(), tbName.Text, Convert.ToDouble(tbHeight.Text), Convert.ToInt32(tbAge.Text),
                    cbTreatmentLocation.SelectedItem.ToString(), cbEvacuatedTo.SelectedItem.ToString(), rtbInjury.Text));

                MessageBox.Show($"{cData.list_of_FC_reports[0].id}\n" +
                    $"{cData.list_of_FC_reports[0].name}\n" +
                    $"{cData.list_of_FC_reports[0].height.ToString()}\n" +
                    $"{cData.list_of_FC_reports[0].age.ToString()}\n" +
                    $"{cData.list_of_FC_reports[0].hospital}\n" +
                    $"{cData.list_of_FC_reports[0].treatmentLocation}\n" +
                    $"{cData.list_of_FC_reports[0].injury}\n");
                clearFields();
            }
            else
            {
                MessageBox.Show("Invalid Data.");
            }
        }

        private void cbTreatmentLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbTreatmentLocation.SelectedIndex == 1)
            {
                cbEvacuatedTo.Enabled = true;
            }
            else
            {
                cbEvacuatedTo.Enabled = false;
            }
        }
    }
}
