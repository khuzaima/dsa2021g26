﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ambulance_Service_System
{
    class cAmbulance
    {
        public string ID { get; set; }
        public string plateNum { get; set; }
        public DateTime registration_Date { get; set; }
        public DateTime updateData_Date { get; set; }
        public String status { get; set; }

        public cAmbulance()
        {
            ID = "";
            plateNum = "";
            registration_Date = DateTime.MinValue;
            updateData_Date = DateTime.MinValue;
            status = "Unavilable";
        }

        public cAmbulance(string id, string num, DateTime registerDate, DateTime updateDate, string stat)
        {
            ID = id;
            plateNum = num;
            registration_Date = registerDate;
            updateData_Date = updateDate;
            status = stat;
        }
    }
}
