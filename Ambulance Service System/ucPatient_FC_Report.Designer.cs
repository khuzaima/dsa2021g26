﻿
namespace Ambulance_Service_System
{
    partial class ucPatient_FC_Report
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlHeader = new System.Windows.Forms.Panel();
            this.lbTitlePatientFCReport = new System.Windows.Forms.Label();
            this.pnlDetails = new System.Windows.Forms.Panel();
            this.lbAge = new System.Windows.Forms.Label();
            this.lbHeight = new System.Windows.Forms.Label();
            this.lbName = new System.Windows.Forms.Label();
            this.tbAge = new System.Windows.Forms.TextBox();
            this.tbHeight = new System.Windows.Forms.TextBox();
            this.tbName = new System.Windows.Forms.TextBox();
            this.lbEvacuatedTo = new System.Windows.Forms.Label();
            this.cbEvacuatedTo = new System.Windows.Forms.ComboBox();
            this.lbDetails = new System.Windows.Forms.Label();
            this.btnSubmit = new System.Windows.Forms.Button();
            this.lbInjury = new System.Windows.Forms.Label();
            this.rtbInjury = new System.Windows.Forms.RichTextBox();
            this.lbTreatmentLocation = new System.Windows.Forms.Label();
            this.cbTreatmentLocation = new System.Windows.Forms.ComboBox();
            this.pnlBody = new System.Windows.Forms.Panel();
            this.pnlHeader.SuspendLayout();
            this.pnlDetails.SuspendLayout();
            this.pnlBody.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlHeader
            // 
            this.pnlHeader.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pnlHeader.Controls.Add(this.lbTitlePatientFCReport);
            this.pnlHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlHeader.ForeColor = System.Drawing.SystemColors.ControlText;
            this.pnlHeader.Location = new System.Drawing.Point(0, 0);
            this.pnlHeader.Name = "pnlHeader";
            this.pnlHeader.Size = new System.Drawing.Size(837, 64);
            this.pnlHeader.TabIndex = 2;
            // 
            // lbTitlePatientFCReport
            // 
            this.lbTitlePatientFCReport.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lbTitlePatientFCReport.AutoSize = true;
            this.lbTitlePatientFCReport.BackColor = System.Drawing.Color.Transparent;
            this.lbTitlePatientFCReport.Font = new System.Drawing.Font("Imprint MT Shadow", 20F);
            this.lbTitlePatientFCReport.ForeColor = System.Drawing.Color.White;
            this.lbTitlePatientFCReport.Location = new System.Drawing.Point(250, 16);
            this.lbTitlePatientFCReport.Name = "lbTitlePatientFCReport";
            this.lbTitlePatientFCReport.Size = new System.Drawing.Size(345, 32);
            this.lbTitlePatientFCReport.TabIndex = 4;
            this.lbTitlePatientFCReport.Text = "Patient First Contact Report";
            // 
            // pnlDetails
            // 
            this.pnlDetails.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.pnlDetails.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pnlDetails.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlDetails.Controls.Add(this.lbAge);
            this.pnlDetails.Controls.Add(this.lbHeight);
            this.pnlDetails.Controls.Add(this.lbName);
            this.pnlDetails.Controls.Add(this.tbAge);
            this.pnlDetails.Controls.Add(this.tbHeight);
            this.pnlDetails.Controls.Add(this.tbName);
            this.pnlDetails.Controls.Add(this.lbEvacuatedTo);
            this.pnlDetails.Controls.Add(this.cbEvacuatedTo);
            this.pnlDetails.Controls.Add(this.lbDetails);
            this.pnlDetails.Controls.Add(this.btnSubmit);
            this.pnlDetails.Controls.Add(this.lbInjury);
            this.pnlDetails.Controls.Add(this.rtbInjury);
            this.pnlDetails.Controls.Add(this.lbTreatmentLocation);
            this.pnlDetails.Controls.Add(this.cbTreatmentLocation);
            this.pnlDetails.Location = new System.Drawing.Point(126, 6);
            this.pnlDetails.Name = "pnlDetails";
            this.pnlDetails.Size = new System.Drawing.Size(590, 435);
            this.pnlDetails.TabIndex = 26;
            // 
            // lbAge
            // 
            this.lbAge.AutoSize = true;
            this.lbAge.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbAge.Location = new System.Drawing.Point(31, 159);
            this.lbAge.Name = "lbAge";
            this.lbAge.Size = new System.Drawing.Size(33, 17);
            this.lbAge.TabIndex = 33;
            this.lbAge.Text = "Age";
            // 
            // lbHeight
            // 
            this.lbHeight.AutoSize = true;
            this.lbHeight.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbHeight.Location = new System.Drawing.Point(314, 86);
            this.lbHeight.Name = "lbHeight";
            this.lbHeight.Size = new System.Drawing.Size(49, 17);
            this.lbHeight.TabIndex = 32;
            this.lbHeight.Text = "Height";
            // 
            // lbName
            // 
            this.lbName.AutoSize = true;
            this.lbName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbName.Location = new System.Drawing.Point(28, 86);
            this.lbName.Name = "lbName";
            this.lbName.Size = new System.Drawing.Size(45, 17);
            this.lbName.TabIndex = 31;
            this.lbName.Text = "Name";
            // 
            // tbAge
            // 
            this.tbAge.Location = new System.Drawing.Point(34, 179);
            this.tbAge.Name = "tbAge";
            this.tbAge.Size = new System.Drawing.Size(243, 20);
            this.tbAge.TabIndex = 30;
            // 
            // tbHeight
            // 
            this.tbHeight.Location = new System.Drawing.Point(317, 106);
            this.tbHeight.Name = "tbHeight";
            this.tbHeight.Size = new System.Drawing.Size(246, 20);
            this.tbHeight.TabIndex = 29;
            // 
            // tbName
            // 
            this.tbName.Location = new System.Drawing.Point(31, 106);
            this.tbName.Name = "tbName";
            this.tbName.Size = new System.Drawing.Size(246, 20);
            this.tbName.TabIndex = 28;
            // 
            // lbEvacuatedTo
            // 
            this.lbEvacuatedTo.AutoSize = true;
            this.lbEvacuatedTo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbEvacuatedTo.Location = new System.Drawing.Point(314, 241);
            this.lbEvacuatedTo.Name = "lbEvacuatedTo";
            this.lbEvacuatedTo.Size = new System.Drawing.Size(96, 17);
            this.lbEvacuatedTo.TabIndex = 26;
            this.lbEvacuatedTo.Text = "Evacuated To";
            // 
            // cbEvacuatedTo
            // 
            this.cbEvacuatedTo.FormattingEnabled = true;
            this.cbEvacuatedTo.Items.AddRange(new object[] {
            "Shaukat Khanum Memorial Hospital",
            "Fatima Memorial Hospital",
            "Combined Medical Hospital",
            "Bahria Town Hospital",
            "Hameed Latif Hospital",
            "Jinnah Hospital",
            "Shaikh Zayed Hospital",
            "Mayo Hospital, Lahore",
            "Sir Ganga Ram Hospital",
            "Doctors Hospital",
            "Shalamar hospital",
            "National Hospital & Medical Center",
            "Masood Hospital",
            "Surgimed Hospital Lahore"});
            this.cbEvacuatedTo.Location = new System.Drawing.Point(317, 261);
            this.cbEvacuatedTo.Name = "cbEvacuatedTo";
            this.cbEvacuatedTo.Size = new System.Drawing.Size(246, 21);
            this.cbEvacuatedTo.TabIndex = 27;
            // 
            // lbDetails
            // 
            this.lbDetails.AutoSize = true;
            this.lbDetails.BackColor = System.Drawing.Color.Transparent;
            this.lbDetails.Font = new System.Drawing.Font("Imprint MT Shadow", 14F);
            this.lbDetails.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lbDetails.Location = new System.Drawing.Point(261, 9);
            this.lbDetails.Name = "lbDetails";
            this.lbDetails.Size = new System.Drawing.Size(67, 23);
            this.lbDetails.TabIndex = 5;
            this.lbDetails.Text = "Details";
            // 
            // btnSubmit
            // 
            this.btnSubmit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSubmit.BackColor = System.Drawing.SystemColors.ControlLight;
            this.btnSubmit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSubmit.Location = new System.Drawing.Point(475, 391);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(102, 30);
            this.btnSubmit.TabIndex = 25;
            this.btnSubmit.Text = "Submit";
            this.btnSubmit.UseVisualStyleBackColor = false;
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            // 
            // lbInjury
            // 
            this.lbInjury.AutoSize = true;
            this.lbInjury.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbInjury.Location = new System.Drawing.Point(31, 241);
            this.lbInjury.Name = "lbInjury";
            this.lbInjury.Size = new System.Drawing.Size(42, 17);
            this.lbInjury.TabIndex = 7;
            this.lbInjury.Text = "Injury";
            // 
            // rtbInjury
            // 
            this.rtbInjury.Location = new System.Drawing.Point(34, 261);
            this.rtbInjury.Name = "rtbInjury";
            this.rtbInjury.Size = new System.Drawing.Size(243, 21);
            this.rtbInjury.TabIndex = 9;
            this.rtbInjury.Text = "";
            // 
            // lbTreatmentLocation
            // 
            this.lbTreatmentLocation.AutoSize = true;
            this.lbTreatmentLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTreatmentLocation.Location = new System.Drawing.Point(314, 159);
            this.lbTreatmentLocation.Name = "lbTreatmentLocation";
            this.lbTreatmentLocation.Size = new System.Drawing.Size(135, 17);
            this.lbTreatmentLocation.TabIndex = 6;
            this.lbTreatmentLocation.Text = "Treatment Location ";
            // 
            // cbTreatmentLocation
            // 
            this.cbTreatmentLocation.FormattingEnabled = true;
            this.cbTreatmentLocation.Items.AddRange(new object[] {
            "On Sites",
            "Hospital"});
            this.cbTreatmentLocation.Location = new System.Drawing.Point(317, 179);
            this.cbTreatmentLocation.Name = "cbTreatmentLocation";
            this.cbTreatmentLocation.Size = new System.Drawing.Size(246, 21);
            this.cbTreatmentLocation.TabIndex = 8;
            this.cbTreatmentLocation.SelectedIndexChanged += new System.EventHandler(this.cbTreatmentLocation_SelectedIndexChanged);
            // 
            // pnlBody
            // 
            this.pnlBody.Controls.Add(this.pnlDetails);
            this.pnlBody.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlBody.Location = new System.Drawing.Point(0, 64);
            this.pnlBody.Name = "pnlBody";
            this.pnlBody.Size = new System.Drawing.Size(837, 460);
            this.pnlBody.TabIndex = 3;
            // 
            // ucPatient_FC_Report
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pnlBody);
            this.Controls.Add(this.pnlHeader);
            this.Name = "ucPatient_FC_Report";
            this.Size = new System.Drawing.Size(837, 524);
            this.pnlHeader.ResumeLayout(false);
            this.pnlHeader.PerformLayout();
            this.pnlDetails.ResumeLayout(false);
            this.pnlDetails.PerformLayout();
            this.pnlBody.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlHeader;
        private System.Windows.Forms.Label lbTitlePatientFCReport;
        private System.Windows.Forms.Panel pnlDetails;
        private System.Windows.Forms.Label lbAge;
        private System.Windows.Forms.Label lbHeight;
        private System.Windows.Forms.Label lbName;
        private System.Windows.Forms.TextBox tbAge;
        private System.Windows.Forms.TextBox tbHeight;
        private System.Windows.Forms.TextBox tbName;
        private System.Windows.Forms.Label lbEvacuatedTo;
        private System.Windows.Forms.ComboBox cbEvacuatedTo;
        private System.Windows.Forms.Label lbDetails;
        private System.Windows.Forms.Button btnSubmit;
        private System.Windows.Forms.Label lbInjury;
        private System.Windows.Forms.RichTextBox rtbInjury;
        private System.Windows.Forms.Label lbTreatmentLocation;
        private System.Windows.Forms.ComboBox cbTreatmentLocation;
        private System.Windows.Forms.Panel pnlBody;
    }
}
