﻿
namespace Ambulance_Service_System
{
    partial class LoginWindow
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LoginWindow));
            this.pnlMain = new System.Windows.Forms.Panel();
            this.lbLoginTxt = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnHidePassword = new System.Windows.Forms.Button();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.lbInvalidTxt = new System.Windows.Forms.Label();
            this.txtPasswordField = new System.Windows.Forms.TextBox();
            this.lbLockIcon = new System.Windows.Forms.Label();
            this.txtEmailField = new System.Windows.Forms.TextBox();
            this.lbUserIcon = new System.Windows.Forms.Label();
            this.rbtnExit = new Ambulance_Service_System.customComponents.RoundButton();
            this.rbtnLogin = new Ambulance_Service_System.customComponents.RoundButton();
            this.pnlMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlMain
            // 
            this.pnlMain.BackColor = System.Drawing.SystemColors.Control;
            this.pnlMain.Controls.Add(this.lbLoginTxt);
            this.pnlMain.Controls.Add(this.panel1);
            this.pnlMain.Controls.Add(this.rbtnExit);
            this.pnlMain.Controls.Add(this.rbtnLogin);
            this.pnlMain.Controls.Add(this.btnHidePassword);
            this.pnlMain.Controls.Add(this.flowLayoutPanel2);
            this.pnlMain.Controls.Add(this.flowLayoutPanel1);
            this.pnlMain.Controls.Add(this.lbInvalidTxt);
            this.pnlMain.Controls.Add(this.txtPasswordField);
            this.pnlMain.Controls.Add(this.lbLockIcon);
            this.pnlMain.Controls.Add(this.txtEmailField);
            this.pnlMain.Controls.Add(this.lbUserIcon);
            this.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMain.Location = new System.Drawing.Point(0, 0);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(344, 578);
            this.pnlMain.TabIndex = 0;
            // 
            // lbLoginTxt
            // 
            this.lbLoginTxt.AutoSize = true;
            this.lbLoginTxt.Font = new System.Drawing.Font("Imprint MT Shadow", 18F);
            this.lbLoginTxt.ForeColor = System.Drawing.Color.Red;
            this.lbLoginTxt.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.lbLoginTxt.Location = new System.Drawing.Point(132, 146);
            this.lbLoginTxt.Name = "lbLoginTxt";
            this.lbLoginTxt.Size = new System.Drawing.Size(83, 28);
            this.lbLoginTxt.TabIndex = 30;
            this.lbLoginTxt.Text = "Log In";
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel1.BackgroundImage")));
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel1.Location = new System.Drawing.Point(70, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(200, 131);
            this.panel1.TabIndex = 29;
            // 
            // btnHidePassword
            // 
            this.btnHidePassword.FlatAppearance.BorderSize = 0;
            this.btnHidePassword.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnHidePassword.Image = ((System.Drawing.Image)(resources.GetObject("btnHidePassword.Image")));
            this.btnHidePassword.Location = new System.Drawing.Point(284, 323);
            this.btnHidePassword.Name = "btnHidePassword";
            this.btnHidePassword.Size = new System.Drawing.Size(22, 22);
            this.btnHidePassword.TabIndex = 23;
            this.btnHidePassword.TabStop = false;
            this.btnHidePassword.UseVisualStyleBackColor = true;
            this.btnHidePassword.Click += new System.EventHandler(this.btnHidePassword_Click);
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(41, 350);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(269, 2);
            this.flowLayoutPanel2.TabIndex = 21;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(41, 278);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(269, 2);
            this.flowLayoutPanel1.TabIndex = 22;
            // 
            // lbInvalidTxt
            // 
            this.lbInvalidTxt.AutoSize = true;
            this.lbInvalidTxt.ForeColor = System.Drawing.Color.Red;
            this.lbInvalidTxt.Location = new System.Drawing.Point(213, 499);
            this.lbInvalidTxt.Name = "lbInvalidTxt";
            this.lbInvalidTxt.Size = new System.Drawing.Size(93, 13);
            this.lbInvalidTxt.TabIndex = 20;
            this.lbInvalidTxt.Text = "Invalid Credentials";
            // 
            // txtPasswordField
            // 
            this.txtPasswordField.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(249)))), ((int)(((byte)(246)))));
            this.txtPasswordField.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtPasswordField.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.txtPasswordField.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.txtPasswordField.Location = new System.Drawing.Point(69, 323);
            this.txtPasswordField.Name = "txtPasswordField";
            this.txtPasswordField.Size = new System.Drawing.Size(214, 22);
            this.txtPasswordField.TabIndex = 19;
            this.txtPasswordField.Text = "Password";
            this.txtPasswordField.Enter += new System.EventHandler(this.txtPasswordField_Enter);
            this.txtPasswordField.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtPasswordField_KeyDown);
            this.txtPasswordField.Leave += new System.EventHandler(this.txtPasswordField_Leave);
            // 
            // lbLockIcon
            // 
            this.lbLockIcon.Font = new System.Drawing.Font("Segoe UI Semibold", 15.75F, System.Drawing.FontStyle.Bold);
            this.lbLockIcon.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(156)))), ((int)(((byte)(208)))));
            this.lbLockIcon.Image = ((System.Drawing.Image)(resources.GetObject("lbLockIcon.Image")));
            this.lbLockIcon.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.lbLockIcon.Location = new System.Drawing.Point(35, 318);
            this.lbLockIcon.Name = "lbLockIcon";
            this.lbLockIcon.Size = new System.Drawing.Size(30, 30);
            this.lbLockIcon.TabIndex = 18;
            // 
            // txtEmailField
            // 
            this.txtEmailField.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(249)))), ((int)(((byte)(246)))));
            this.txtEmailField.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtEmailField.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.txtEmailField.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.txtEmailField.Location = new System.Drawing.Point(69, 251);
            this.txtEmailField.Name = "txtEmailField";
            this.txtEmailField.Size = new System.Drawing.Size(239, 22);
            this.txtEmailField.TabIndex = 17;
            this.txtEmailField.Text = "Username";
            this.txtEmailField.Enter += new System.EventHandler(this.txtEmailField_Enter);
            this.txtEmailField.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtEmailField_KeyDown);
            this.txtEmailField.Leave += new System.EventHandler(this.txtEmailField_Leave);
            // 
            // lbUserIcon
            // 
            this.lbUserIcon.Font = new System.Drawing.Font("Segoe UI Semibold", 15.75F, System.Drawing.FontStyle.Bold);
            this.lbUserIcon.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(156)))), ((int)(((byte)(208)))));
            this.lbUserIcon.Image = ((System.Drawing.Image)(resources.GetObject("lbUserIcon.Image")));
            this.lbUserIcon.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.lbUserIcon.Location = new System.Drawing.Point(35, 247);
            this.lbUserIcon.Name = "lbUserIcon";
            this.lbUserIcon.Size = new System.Drawing.Size(30, 30);
            this.lbUserIcon.TabIndex = 16;
            // 
            // rbtnExit
            // 
            this.rbtnExit.BackColor = System.Drawing.SystemColors.Control;
            this.rbtnExit.BackgroundColor = System.Drawing.SystemColors.Control;
            this.rbtnExit.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(40)))), ((int)(((byte)(50)))));
            this.rbtnExit.BorderRadius = 20;
            this.rbtnExit.BorderSize = 2;
            this.rbtnExit.FlatAppearance.BorderSize = 0;
            this.rbtnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rbtnExit.ForeColor = System.Drawing.SystemColors.ControlText;
            this.rbtnExit.Image = ((System.Drawing.Image)(resources.GetObject("rbtnExit.Image")));
            this.rbtnExit.Location = new System.Drawing.Point(41, 515);
            this.rbtnExit.Name = "rbtnExit";
            this.rbtnExit.Size = new System.Drawing.Size(100, 40);
            this.rbtnExit.TabIndex = 28;
            this.rbtnExit.Text = "Exit";
            this.rbtnExit.TextColor = System.Drawing.SystemColors.ControlText;
            this.rbtnExit.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.rbtnExit.UseVisualStyleBackColor = false;
            this.rbtnExit.Click += new System.EventHandler(this.rbtnExit_Click);
            this.rbtnExit.MouseEnter += new System.EventHandler(this.rbtnExit_MouseEnter);
            this.rbtnExit.MouseLeave += new System.EventHandler(this.rbtnExit_MouseLeave);
            // 
            // rbtnLogin
            // 
            this.rbtnLogin.BackColor = System.Drawing.SystemColors.Control;
            this.rbtnLogin.BackgroundColor = System.Drawing.SystemColors.Control;
            this.rbtnLogin.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(70)))), ((int)(((byte)(230)))));
            this.rbtnLogin.BorderRadius = 20;
            this.rbtnLogin.BorderSize = 2;
            this.rbtnLogin.FlatAppearance.BorderSize = 0;
            this.rbtnLogin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rbtnLogin.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtnLogin.ForeColor = System.Drawing.SystemColors.ControlText;
            this.rbtnLogin.Image = ((System.Drawing.Image)(resources.GetObject("rbtnLogin.Image")));
            this.rbtnLogin.Location = new System.Drawing.Point(210, 515);
            this.rbtnLogin.Name = "rbtnLogin";
            this.rbtnLogin.Size = new System.Drawing.Size(100, 40);
            this.rbtnLogin.TabIndex = 27;
            this.rbtnLogin.Text = "Login";
            this.rbtnLogin.TextColor = System.Drawing.SystemColors.ControlText;
            this.rbtnLogin.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.rbtnLogin.UseVisualStyleBackColor = false;
            this.rbtnLogin.Click += new System.EventHandler(this.rbtnLogin_Click);
            this.rbtnLogin.MouseEnter += new System.EventHandler(this.rbtnLogin_MouseEnter);
            this.rbtnLogin.MouseLeave += new System.EventHandler(this.rbtnLogin_MouseLeave);
            // 
            // LoginWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(344, 578);
            this.Controls.Add(this.pnlMain);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "LoginWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Login";
            this.VisibleChanged += new System.EventHandler(this.LoginWindow_VisibleChanged);
            this.pnlMain.ResumeLayout(false);
            this.pnlMain.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlMain;
        private System.Windows.Forms.Button btnHidePassword;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Label lbInvalidTxt;
        private System.Windows.Forms.TextBox txtPasswordField;
        private System.Windows.Forms.Label lbLockIcon;
        private System.Windows.Forms.TextBox txtEmailField;
        private System.Windows.Forms.Label lbUserIcon;
        private customComponents.RoundButton rbtnExit;
        private customComponents.RoundButton rbtnLogin;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lbLoginTxt;
    }
}

