﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ambulance_Service_System
{
    public partial class ucHospitals : UserControl
    {
        private static ucHospitals _instence;
        public static ucHospitals Instence
        {
            get
            {
                if (_instence == null)
                {
                    _instence = new ucHospitals();
                }
                return _instence;
            }
        }
        public ucHospitals()
        {
            InitializeComponent();
            LoadDataInDtv();
        }

        public void RefreshUC()
        {
            tbSearch.Text = "";
            LoadDataInDtv();
        }

        private void LoadDataInDtv()
        {
            dtvFC_Reports.Rows.Clear();
            for (int i = 0; i < cData.list_of_FC_reports.Count; i++)
            {
                dtvFC_Reports.Rows.Add(cData.list_of_FC_reports[i].id, cData.list_of_FC_reports[i].hospital);
            }
        }

        public int getObjectIndex(string id)
        {
            for (int i = 0; i < cData.list_of_FC_reports.Count; i++)
            {
                if (cData.list_of_FC_reports[i].id == id)
                {
                    return i;
                }
            }
            return -1;
        }

        private void tbSearch_TextChanged(object sender, EventArgs e)
        {
            dtvFC_Reports.Rows.Clear();
            foreach (var item in cData.list_of_FC_reports)
            {
                if (item.id.Contains(tbSearch.Text) || item.hospital.Contains(tbSearch.Text))
                {
                    dtvFC_Reports.Rows.Add(item.id, item.hospital);
                }
            }
        }

        private void dtvFC_Reports_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex>=0)
            {
                int index = getObjectIndex(dtvFC_Reports.Rows[e.RowIndex].Cells[0].Value.ToString());
                if (e.ColumnIndex==2)
                {
                    cFC_Report report = cData.list_of_FC_reports[index];
                    MessageBox.Show($"ID : {report.id}\n" +
                        $"name : {report.name}\n" +
                        $"height : {report.height}\n" +
                        $"age : {report.age}\n" +
                        $"hospital : {report.hospital}\n" +
                        $"injury : {report.injury}");    
                }
                if (e.ColumnIndex == 3)
                {

                }
                if (e.ColumnIndex == 4)
                {

                }
            }
        }
    }
}
