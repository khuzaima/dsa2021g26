﻿
namespace Ambulance_Service_System
{
    partial class ucDispatchAmbulance
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlHeader = new System.Windows.Forms.Panel();
            this.lbTitleDispatch = new System.Windows.Forms.Label();
            this.pnlCallDetails = new System.Windows.Forms.Panel();
            this.btnDisptach = new System.Windows.Forms.Button();
            this.rtbLocation = new System.Windows.Forms.RichTextBox();
            this.cbAccidentType = new System.Windows.Forms.ComboBox();
            this.lbLocation = new System.Windows.Forms.Label();
            this.lbAccidentType = new System.Windows.Forms.Label();
            this.lbCallDetails = new System.Windows.Forms.Label();
            this.pnlAmbulancesAvaliable = new System.Windows.Forms.Panel();
            this.lbAmbulancesAvailable = new System.Windows.Forms.Label();
            this.dtvAmbulancesAvailable = new System.Windows.Forms.DataGridView();
            this.DispatchOrder = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AmbulanceID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AmbulancePlateNum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlHeader.SuspendLayout();
            this.pnlCallDetails.SuspendLayout();
            this.pnlAmbulancesAvaliable.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtvAmbulancesAvailable)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlHeader
            // 
            this.pnlHeader.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pnlHeader.Controls.Add(this.lbTitleDispatch);
            this.pnlHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlHeader.ForeColor = System.Drawing.SystemColors.ControlText;
            this.pnlHeader.Location = new System.Drawing.Point(0, 0);
            this.pnlHeader.Name = "pnlHeader";
            this.pnlHeader.Size = new System.Drawing.Size(837, 64);
            this.pnlHeader.TabIndex = 0;
            // 
            // lbTitleDispatch
            // 
            this.lbTitleDispatch.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lbTitleDispatch.AutoSize = true;
            this.lbTitleDispatch.BackColor = System.Drawing.Color.Transparent;
            this.lbTitleDispatch.Font = new System.Drawing.Font("Imprint MT Shadow", 20F);
            this.lbTitleDispatch.ForeColor = System.Drawing.Color.White;
            this.lbTitleDispatch.Location = new System.Drawing.Point(335, 16);
            this.lbTitleDispatch.Name = "lbTitleDispatch";
            this.lbTitleDispatch.Size = new System.Drawing.Size(167, 32);
            this.lbTitleDispatch.TabIndex = 4;
            this.lbTitleDispatch.Text = "DISPATCH";
            // 
            // pnlCallDetails
            // 
            this.pnlCallDetails.Controls.Add(this.btnDisptach);
            this.pnlCallDetails.Controls.Add(this.rtbLocation);
            this.pnlCallDetails.Controls.Add(this.cbAccidentType);
            this.pnlCallDetails.Controls.Add(this.lbLocation);
            this.pnlCallDetails.Controls.Add(this.lbAccidentType);
            this.pnlCallDetails.Controls.Add(this.lbCallDetails);
            this.pnlCallDetails.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlCallDetails.Location = new System.Drawing.Point(0, 64);
            this.pnlCallDetails.Name = "pnlCallDetails";
            this.pnlCallDetails.Size = new System.Drawing.Size(837, 159);
            this.pnlCallDetails.TabIndex = 1;
            // 
            // btnDisptach
            // 
            this.btnDisptach.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDisptach.BackColor = System.Drawing.SystemColors.ControlLight;
            this.btnDisptach.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDisptach.Location = new System.Drawing.Point(719, 113);
            this.btnDisptach.Name = "btnDisptach";
            this.btnDisptach.Size = new System.Drawing.Size(102, 30);
            this.btnDisptach.TabIndex = 25;
            this.btnDisptach.Text = "Dispatch";
            this.btnDisptach.UseVisualStyleBackColor = false;
            this.btnDisptach.Click += new System.EventHandler(this.btnDisptach_Click);
            // 
            // rtbLocation
            // 
            this.rtbLocation.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rtbLocation.Location = new System.Drawing.Point(300, 68);
            this.rtbLocation.Name = "rtbLocation";
            this.rtbLocation.Size = new System.Drawing.Size(410, 21);
            this.rtbLocation.TabIndex = 9;
            this.rtbLocation.Text = "";
            // 
            // cbAccidentType
            // 
            this.cbAccidentType.FormattingEnabled = true;
            this.cbAccidentType.Items.AddRange(new object[] {
            "Road Accident",
            "Fire OutBurst",
            "Building Collapse",
            "Sick at Home",
            "Drowning"});
            this.cbAccidentType.Location = new System.Drawing.Point(13, 68);
            this.cbAccidentType.Name = "cbAccidentType";
            this.cbAccidentType.Size = new System.Drawing.Size(246, 21);
            this.cbAccidentType.TabIndex = 8;
            // 
            // lbLocation
            // 
            this.lbLocation.AutoSize = true;
            this.lbLocation.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbLocation.Location = new System.Drawing.Point(297, 48);
            this.lbLocation.Name = "lbLocation";
            this.lbLocation.Size = new System.Drawing.Size(62, 17);
            this.lbLocation.TabIndex = 7;
            this.lbLocation.Text = "Location";
            // 
            // lbAccidentType
            // 
            this.lbAccidentType.AutoSize = true;
            this.lbAccidentType.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbAccidentType.Location = new System.Drawing.Point(10, 48);
            this.lbAccidentType.Name = "lbAccidentType";
            this.lbAccidentType.Size = new System.Drawing.Size(98, 17);
            this.lbAccidentType.TabIndex = 6;
            this.lbAccidentType.Text = "Accident Type";
            // 
            // lbCallDetails
            // 
            this.lbCallDetails.AutoSize = true;
            this.lbCallDetails.BackColor = System.Drawing.Color.Transparent;
            this.lbCallDetails.Font = new System.Drawing.Font("Imprint MT Shadow", 14F);
            this.lbCallDetails.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lbCallDetails.Location = new System.Drawing.Point(3, 3);
            this.lbCallDetails.Name = "lbCallDetails";
            this.lbCallDetails.Size = new System.Drawing.Size(105, 23);
            this.lbCallDetails.TabIndex = 5;
            this.lbCallDetails.Text = "Call Details";
            // 
            // pnlAmbulancesAvaliable
            // 
            this.pnlAmbulancesAvaliable.Controls.Add(this.lbAmbulancesAvailable);
            this.pnlAmbulancesAvaliable.Controls.Add(this.dtvAmbulancesAvailable);
            this.pnlAmbulancesAvaliable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlAmbulancesAvaliable.Location = new System.Drawing.Point(0, 223);
            this.pnlAmbulancesAvaliable.Name = "pnlAmbulancesAvaliable";
            this.pnlAmbulancesAvaliable.Size = new System.Drawing.Size(837, 301);
            this.pnlAmbulancesAvaliable.TabIndex = 2;
            // 
            // lbAmbulancesAvailable
            // 
            this.lbAmbulancesAvailable.AutoSize = true;
            this.lbAmbulancesAvailable.BackColor = System.Drawing.Color.Transparent;
            this.lbAmbulancesAvailable.Font = new System.Drawing.Font("Imprint MT Shadow", 14F);
            this.lbAmbulancesAvailable.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lbAmbulancesAvailable.Location = new System.Drawing.Point(3, 3);
            this.lbAmbulancesAvailable.Name = "lbAmbulancesAvailable";
            this.lbAmbulancesAvailable.Size = new System.Drawing.Size(193, 23);
            this.lbAmbulancesAvailable.TabIndex = 10;
            this.lbAmbulancesAvailable.Text = "Ambulances Available";
            // 
            // dtvAmbulancesAvailable
            // 
            this.dtvAmbulancesAvailable.AllowUserToAddRows = false;
            this.dtvAmbulancesAvailable.AllowUserToDeleteRows = false;
            this.dtvAmbulancesAvailable.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtvAmbulancesAvailable.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dtvAmbulancesAvailable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtvAmbulancesAvailable.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.DispatchOrder,
            this.AmbulanceID,
            this.AmbulancePlateNum});
            this.dtvAmbulancesAvailable.Location = new System.Drawing.Point(7, 29);
            this.dtvAmbulancesAvailable.Name = "dtvAmbulancesAvailable";
            this.dtvAmbulancesAvailable.ReadOnly = true;
            this.dtvAmbulancesAvailable.Size = new System.Drawing.Size(824, 257);
            this.dtvAmbulancesAvailable.TabIndex = 9;
            // 
            // DispatchOrder
            // 
            this.DispatchOrder.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.DispatchOrder.HeaderText = "Dispatch Order";
            this.DispatchOrder.Name = "DispatchOrder";
            this.DispatchOrder.ReadOnly = true;
            // 
            // AmbulanceID
            // 
            this.AmbulanceID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.AmbulanceID.HeaderText = "Ambulance ID";
            this.AmbulanceID.Name = "AmbulanceID";
            this.AmbulanceID.ReadOnly = true;
            // 
            // AmbulancePlateNum
            // 
            this.AmbulancePlateNum.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.AmbulancePlateNum.HeaderText = "Ambulance Plate No.";
            this.AmbulancePlateNum.Name = "AmbulancePlateNum";
            this.AmbulancePlateNum.ReadOnly = true;
            // 
            // ucDispatchAmbulance
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.Controls.Add(this.pnlAmbulancesAvaliable);
            this.Controls.Add(this.pnlCallDetails);
            this.Controls.Add(this.pnlHeader);
            this.Name = "ucDispatchAmbulance";
            this.Size = new System.Drawing.Size(837, 524);
            this.pnlHeader.ResumeLayout(false);
            this.pnlHeader.PerformLayout();
            this.pnlCallDetails.ResumeLayout(false);
            this.pnlCallDetails.PerformLayout();
            this.pnlAmbulancesAvaliable.ResumeLayout(false);
            this.pnlAmbulancesAvaliable.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtvAmbulancesAvailable)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlHeader;
        private System.Windows.Forms.Panel pnlCallDetails;
        private System.Windows.Forms.Panel pnlAmbulancesAvaliable;
        private System.Windows.Forms.Label lbTitleDispatch;
        private System.Windows.Forms.RichTextBox rtbLocation;
        private System.Windows.Forms.ComboBox cbAccidentType;
        private System.Windows.Forms.Label lbLocation;
        private System.Windows.Forms.Label lbAccidentType;
        private System.Windows.Forms.Label lbCallDetails;
        private System.Windows.Forms.Button btnDisptach;
        private System.Windows.Forms.Label lbAmbulancesAvailable;
        private System.Windows.Forms.DataGridView dtvAmbulancesAvailable;
        private System.Windows.Forms.DataGridViewTextBoxColumn DispatchOrder;
        private System.Windows.Forms.DataGridViewTextBoxColumn AmbulanceID;
        private System.Windows.Forms.DataGridViewTextBoxColumn AmbulancePlateNum;
    }
}
