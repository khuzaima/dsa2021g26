﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ambulance_Service_System
{
    public partial class ucStaffShifts : UserControl
    {
        private static ucStaffShifts _instence;
        public static ucStaffShifts Instence
        {
            get
            {
                if (_instence == null)
                {
                    _instence = new ucStaffShifts();
                }
                return _instence;
            }
        }
        public ucStaffShifts()
        {
            InitializeComponent();
            cbChangeShift.SelectedIndex = 0;
            LoadDataInDtv();
        }

        public void RefreshUC()
        {
            tbSearch.Text = "";
            cbChangeShift.SelectedIndex = 0;
            LoadDataInDtv();
        }

        private void LoadDataInDtv()
        {
            dtvShiftStaff.Rows.Clear();
            for (int i = 0; i < cData.list_of_staff.Count; i++)
            {
                if (cData.list_of_staff[i].shift == cbChangeShift.SelectedItem.ToString())
                {
                    dtvShiftStaff.Rows.Add(cData.list_of_staff[i].id, cData.list_of_staff[i].name);
                }
            }
        }

        public int getObjectIndex(string id)
        {
            for (int i = 0; i < cData.list_of_staff.Count; i++)
            {
                if (cData.list_of_staff[i].id == id)
                {
                    return i;
                }
            }
            return -1;
        }

        public void getStaffOfShift()
        {
            dtvShiftStaff.Rows.Clear();
            for (int i = 0; i < cData.list_of_staff.Count; i++)
            {
                if (cData.list_of_staff[i].shift == cbChangeShift.SelectedItem.ToString())
                {
                    dtvShiftStaff.Rows.Add(cData.list_of_staff[i].id, cData.list_of_staff[i].name);
                }
            }
        }

        private void dtvShiftStaff_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex>=0)
            {
                if (e.ColumnIndex == 2)
                {
                    int index = getObjectIndex(dtvShiftStaff.Rows[e.RowIndex].Cells[0].Value.ToString());
                    MessageBox.Show($"Staff id : {cData.list_of_staff[index].id}\n" +
                        $"Staff name : {cData.list_of_staff[index].name}\n" +
                        $"Staff cnic : {cData.list_of_staff[index].cnic}\n" +
                        $"Staff age : {cData.list_of_staff[index].age}\n" +
                        $"Staff category : {cData.list_of_staff[index].category}\n" +
                        $"Staff shift : {cData.list_of_staff[index].shift}\n" +
                        $"Staff email : {cData.list_of_staff[index].email}\n" +
                        $"Staff password : {cData.list_of_staff[index].password}\n");
                }
            }
        }

        private void tbSearch_TextChanged(object sender, EventArgs e)
        {
            dtvShiftStaff.Rows.Clear();
            foreach (var item in cData.list_of_staff)
            {
                if (item.id.Contains(tbSearch.Text) || item.name.Contains(tbSearch.Text))
                {
                    dtvShiftStaff.Rows.Add(item.id, item.name);
                }
            }
        }

        private void cbChangeShift_SelectedIndexChanged(object sender, EventArgs e)
        {
            getStaffOfShift();
        }
    }
}
