﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ambulance_Service_System
{
    public partial class ucDispatchAmbulance : UserControl
    {

        private static ucDispatchAmbulance _instence;

        public static ucDispatchAmbulance Instence
        {
            get
            {
                if (_instence == null)
                {
                    _instence = new ucDispatchAmbulance();
                }
                return _instence;
            }
         }
        public ucDispatchAmbulance()
        {
            InitializeComponent();
            LoadDataInDtv();
        }

        private void LoadDataInDtv()
        {
            dtvAmbulancesAvailable.Rows.Clear();
            List<cAmbulance> list = cData.queue_of_ambulances.ToList();
            for (int i = 0; i < cData.queue_of_ambulances.Count; i++)
            {
                dtvAmbulancesAvailable.Rows.Add(i+1, list[i].ID, list[i].plateNum);
            }
        }

        public void RefreshUC()
        {
            LoadDataInDtv();
        }

        private void btnDisptach_Click(object sender, EventArgs e)
        {
            if (cbAccidentType.SelectedItem!=null && rtbLocation.Text!="")
            {
                cAmbulance ambulance = cData.queue_of_ambulances.Dequeue();
                cDispatchOrder dispatchOrder = new cDispatchOrder(ambulance.ID, cbAccidentType.SelectedItem.ToString(), rtbLocation.Text);
                foreach (var item in cData.list_of_ambulances)
                {
                    if (item.ID == ambulance.ID)
                    {
                        item.status = "On Rescue";
                    }
                }
                MessageBox.Show($"Dispatch Order Generated.\n{ambulance.ID}\n{cbAccidentType.SelectedItem.ToString()}\n{rtbLocation.Text}");
                cData.list_of_dispatch_orders.Add(dispatchOrder);
                cbAccidentType.SelectedItem = null;
                rtbLocation.Text = "";
            }
            LoadDataInDtv();
        }
    }
}
