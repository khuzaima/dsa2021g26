﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ambulance_Service_System
{
    class cDispatchOrder
    {
        public String AmbulanceID { get; set; }
        public String AccidentType { get; set; }
        public String AccidentLocation { get; set; }

        public cDispatchOrder()
        {
            AmbulanceID = "";
            AccidentType = "";
            AccidentLocation = "";
        }

        public cDispatchOrder(String id , String type, String location)
        {
            AmbulanceID = id;
            AccidentType = type;
            AccidentLocation = location;
        }
    }
}
