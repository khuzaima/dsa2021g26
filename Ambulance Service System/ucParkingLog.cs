﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ambulance_Service_System
{
    public partial class ucParkingLog : UserControl
    {
        private static ucParkingLog _instence;
        public static ucParkingLog Instence
        {
            get
            {
                if(_instence == null)
                {
                    _instence = new ucParkingLog();
                }
                return _instence;
            }
        }
        public ucParkingLog()
        {
            InitializeComponent();
            LoadDataInDtv();
        }
        public void RefreshUC()
        {
            tbSearch.Text = "";
            LoadDataInDtv();
        }

        private void LoadDataInDtv()
        {
            dtvParkingLog.Rows.Clear();
            for (int i = 0; i < cData.list_of_parking_reports.Count; i++)
            {
                dtvParkingLog.Rows.Add(cData.list_of_parking_reports[i].ambID, cData.list_of_parking_reports[i].ambPlateNum, cData.list_of_parking_reports[i].dateTime,
                    cData.list_of_parking_reports[i].status);
            }
        }

        private void tbSearch_TextChanged(object sender, EventArgs e)
        {
            dtvParkingLog.Rows.Clear();
            foreach (var item in cData.list_of_parking_reports)
            {
                if (item.ambID.Contains(tbSearch.Text) || item.ambPlateNum.Contains(tbSearch.Text))
                {
                    dtvParkingLog.Rows.Add(item.ambID, item.ambPlateNum, item.dateTime, item.status);
                }
            }
        }
    }
}
