﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ambulance_Service_System
{
    class cData
    {
        private static cData _instence;

        public static List<cAmbulance> list_of_ambulances { get; set; }
        public static Queue<cAmbulance> queue_of_ambulances { get; set; }
        public static List<cDispatchOrder> list_of_dispatch_orders { get; set; }
        public static List<cFC_Report> list_of_FC_reports { get; set; }
        public static List<cParkingReport> list_of_parking_reports { get; set; }
        public static List<cStaff> list_of_staff { get; set; }

        public static cData Instence
        {
            get
            {
                if(_instence == null)
                {
                    _instence = new cData();
                }
                return _instence;
            }
        }

        public cData()
        {
            list_of_ambulances = new List<cAmbulance>();
            queue_of_ambulances = new Queue<cAmbulance>();
            list_of_dispatch_orders = new List<cDispatchOrder>();
            list_of_FC_reports = new List<cFC_Report>();
            list_of_parking_reports = new List<cParkingReport>();
            list_of_staff = new List<cStaff>();
        }

        public void load_Ambulances_Data()
        {
            try
            {
                StreamReader reader = new StreamReader(@"Ambulances_Data.csv");
                String line = reader.ReadLine();
                while (line != null)
                {
                    String[] arr = line.Split(',');
                    cAmbulance ambulance = new cAmbulance(arr[0], arr[1], Convert.ToDateTime(arr[2]), Convert.ToDateTime(arr[3]), arr[4]);
                    list_of_ambulances.Add(ambulance);
                    line = reader.ReadLine();
                }
                 reader.Close();
            }
            catch (Exception ex)
            {
            }
        }

        public void Save_Ambulances_Data()
        {
            try
            {
                StreamWriter writer = new StreamWriter(@"Ambulances_Data.csv");
                for (int i = 0; i < list_of_ambulances.Count; i++)
                {
                    cAmbulance ambulance = list_of_ambulances[i];
                    writer.WriteLine($"{ambulance.ID},{ambulance.plateNum},{ambulance.registration_Date},{ambulance.updateData_Date},{ambulance.status}");
                }
                writer.Flush();
                writer.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error Saving the file Ambulances_Data.csv");
            }
        }

        public void load_Ambulances_Queue_Data()
        {
            try
            {
                StreamReader reader = new StreamReader(@"Ambulances_Queue_Data.csv");
                String line = reader.ReadLine();
                while (line != null)
                {
                    String[] arr = line.Split(',');
                    cAmbulance ambulance = new cAmbulance(arr[0], arr[1], Convert.ToDateTime(arr[2]), Convert.ToDateTime(arr[3]), arr[4]);
                    queue_of_ambulances.Enqueue(ambulance);
                    line = reader.ReadLine();
                }
                reader.Close();
            }
            catch (Exception ex)
            {
            }
        }

        public void Save_Ambulances_Queue_Data()
        {
            try
            {
                StreamWriter writer = new StreamWriter(@"Ambulances_Queue_Data.csv");
                int count = queue_of_ambulances.Count;
                for (int i = 0; i < count; i++)
                {
                    cAmbulance ambulance = queue_of_ambulances.Dequeue();
                    writer.WriteLine($"{ambulance.ID},{ambulance.plateNum},{ambulance.registration_Date},{ambulance.updateData_Date},{ambulance.status}");
                }
                writer.Flush();
                writer.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error Saving the file Ambulances_Queue_Data.csv");
            }
        }

        public void load_Dispatch_Order_Data()
        {
            try
            {
                StreamReader reader = new StreamReader(@"Dispatch_Order_Data.csv");
                String line = reader.ReadLine();
                while (line != null)
                {
                    String[] arr = line.Split(',');
                    cDispatchOrder dispatchOrder = new cDispatchOrder(arr[0], arr[1], arr[2]);
                    list_of_dispatch_orders.Add(dispatchOrder);
                    line = reader.ReadLine();
                }
                reader.Close();
            }
            catch (Exception ex)
            {
            }
        }

        public void save_Dispatch_Order_Data()
        {
            try
            {
                StreamWriter writer = new StreamWriter(@"Dispatch_Order_Data.csv");
                for (int i = 0; i < list_of_dispatch_orders.Count; i++)
                {
                    cDispatchOrder dispatchOrder = list_of_dispatch_orders[i];
                    writer.WriteLine($"{dispatchOrder.AmbulanceID},{dispatchOrder.AccidentType},{dispatchOrder.AccidentLocation}");
                }
                writer.Flush();
                writer.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error Saving the file Dispatch_Order_Data.csv");
            }
        }

        public void load_FC_Reports_Data()
        {
            try
            {
                StreamReader reader = new StreamReader(@"FC_Reports_Data.csv");
                String line = reader.ReadLine();
                while (line != null)
                {
                    String[] arr = line.Split(',');
                    cFC_Report fC_Report = new cFC_Report(arr[0], arr[1], Convert.ToDouble(arr[2]),Convert.ToInt32(arr[3]) ,arr[4],arr[5],arr[6]);
                    list_of_FC_reports.Add(fC_Report);
                    line = reader.ReadLine();
                }
                reader.Close();
            }
            catch (Exception ex)
            {
            }
        }

        public void save_FC_Reports_Data()
        {
            try
            {
                StreamWriter writer = new StreamWriter(@"FC_Reports_Data.csv");
                for (int i = 0; i < list_of_FC_reports.Count; i++)
                {
                    cFC_Report fC_Report = list_of_FC_reports[i];
                    writer.WriteLine($"{fC_Report.id},{fC_Report.name},{fC_Report.height},{fC_Report.age},{fC_Report.treatmentLocation},{fC_Report.hospital},{fC_Report.injury}");
                }
                writer.Flush();
                writer.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error Saving the file FC_Reports_Data.csv");
            }
        }

        public void load_Parking_Reports_Data()
        {
            try
            {
                StreamReader reader = new StreamReader(@"Parking_Reports_Data.csv");
                String line = reader.ReadLine();
                while (line != null)
                {
                    String[] arr = line.Split(',');
                    cParkingReport parkingReport = new cParkingReport(arr[0], arr[1], Convert.ToDateTime(arr[2]), arr[3]);
                    list_of_parking_reports.Add(parkingReport);
                    line = reader.ReadLine();
                }
                reader.Close();
            }
            catch (Exception ex)
            {
            }
        }

        public void save_Parking_Reports_Data()
        {
            try
            {
                StreamWriter writer = new StreamWriter(@"Parking_Reports_Data.csv");
                for (int i = 0; i < list_of_parking_reports.Count; i++)
                {
                    cParkingReport parkingReport = list_of_parking_reports[i];
                    writer.WriteLine($"{parkingReport.ambID},{parkingReport.ambPlateNum},{parkingReport.dateTime},{parkingReport.status}");
                }
                writer.Flush();
                writer.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error Saving the file Parking_Reports_Data.csv");
            }
        }

        public void load_Staff_Data()
        {
            try
            {
                StreamReader reader = new StreamReader(@"Staff_Data.csv");
                String line = reader.ReadLine();
                while (line != null)
                {
                    String[] arr = line.Split(',');
                    cStaff staff = new cStaff(arr[0], arr[1], Convert.ToInt32(arr[2]), arr[3], arr[4], arr[5], arr[6], arr[7]);
                    list_of_staff.Add(staff);
                    line = reader.ReadLine();
                }
                reader.Close();
            }
            catch (Exception ex)
            {
            }
        }

        public void save_Staff_Data()
        {
            try
            {
                StreamWriter writer = new StreamWriter(@"Staff_Data.csv");
                for (int i = 0; i < list_of_staff.Count; i++)
                {
                    cStaff staff = list_of_staff[i];
                    writer.WriteLine($"{staff.name},{staff.cnic},{staff.age},{staff.category},{staff.shift},{staff.id},{staff.email},{staff.password}");
                }
                writer.Flush();
                writer.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error Saving the file Staff_Data.csv");
            }
        }

    }
}
