﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ambulance_Service_System
{
    class cStaff
    {
        public string id { get; set; }
        public string name { get; set; }
        public string cnic { get; set; }
        public int age { get; set; }
        public string category { get; set; }
        public string shift { get; set; }
        public string email { get; set; }
        public string password { get; set; }

        public cStaff()
        {
            id = "";
            name = "";
            cnic = "";
            age = 0;
            category = "";
            shift = "";
            email = "";
            password = "";
        }

        public cStaff(string nam, string cnc, int ag, string categry, string shif, string i, string mail, string pass)
        {
            id = i;
            name = nam;
            cnic = cnc;
            age = ag;
            category = categry;
            shift = shif;
            email = mail;
            password = pass;
        }
    }
}
