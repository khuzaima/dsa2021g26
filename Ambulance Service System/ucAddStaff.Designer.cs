﻿
namespace Ambulance_Service_System
{
    partial class ucAddStaff
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlStaff = new System.Windows.Forms.Panel();
            this.dtvShiftStaff = new System.Windows.Forms.DataGridView();
            this.lbSearch = new System.Windows.Forms.Label();
            this.tbSearch = new System.Windows.Forms.TextBox();
            this.lbStaff = new System.Windows.Forms.Label();
            this.pnlAddStaffSection = new System.Windows.Forms.Panel();
            this.pnlAddNewStaff = new System.Windows.Forms.Panel();
            this.mtbCnic = new System.Windows.Forms.MaskedTextBox();
            this.tbAge = new System.Windows.Forms.TextBox();
            this.lbAge = new System.Windows.Forms.Label();
            this.lbCnic = new System.Windows.Forms.Label();
            this.tbName = new System.Windows.Forms.TextBox();
            this.cbShift = new System.Windows.Forms.ComboBox();
            this.lbShift = new System.Windows.Forms.Label();
            this.btnAdd = new System.Windows.Forms.Button();
            this.cbStaffCategory = new System.Windows.Forms.ComboBox();
            this.lbName = new System.Windows.Forms.Label();
            this.lbType = new System.Windows.Forms.Label();
            this.lbStaffDetails = new System.Windows.Forms.Label();
            this.pnlHeader = new System.Windows.Forms.Panel();
            this.lbTitleAddStaff = new System.Windows.Forms.Label();
            this.StaffID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StaffName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.View = new System.Windows.Forms.DataGridViewButtonColumn();
            this.Edit = new System.Windows.Forms.DataGridViewButtonColumn();
            this.Delete = new System.Windows.Forms.DataGridViewButtonColumn();
            this.pnlStaff.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtvShiftStaff)).BeginInit();
            this.pnlAddStaffSection.SuspendLayout();
            this.pnlAddNewStaff.SuspendLayout();
            this.pnlHeader.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlStaff
            // 
            this.pnlStaff.Controls.Add(this.dtvShiftStaff);
            this.pnlStaff.Controls.Add(this.lbSearch);
            this.pnlStaff.Controls.Add(this.tbSearch);
            this.pnlStaff.Controls.Add(this.lbStaff);
            this.pnlStaff.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlStaff.Location = new System.Drawing.Point(0, 223);
            this.pnlStaff.Name = "pnlStaff";
            this.pnlStaff.Size = new System.Drawing.Size(837, 301);
            this.pnlStaff.TabIndex = 5;
            // 
            // dtvShiftStaff
            // 
            this.dtvShiftStaff.AllowUserToAddRows = false;
            this.dtvShiftStaff.AllowUserToDeleteRows = false;
            this.dtvShiftStaff.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtvShiftStaff.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dtvShiftStaff.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtvShiftStaff.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.StaffID,
            this.StaffName,
            this.View,
            this.Edit,
            this.Delete});
            this.dtvShiftStaff.Location = new System.Drawing.Point(7, 53);
            this.dtvShiftStaff.Name = "dtvShiftStaff";
            this.dtvShiftStaff.ReadOnly = true;
            this.dtvShiftStaff.Size = new System.Drawing.Size(824, 219);
            this.dtvShiftStaff.TabIndex = 21;
            this.dtvShiftStaff.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtvShiftStaff_CellContentClick);
            // 
            // lbSearch
            // 
            this.lbSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbSearch.AutoSize = true;
            this.lbSearch.Location = new System.Drawing.Point(632, 31);
            this.lbSearch.Name = "lbSearch";
            this.lbSearch.Size = new System.Drawing.Size(41, 13);
            this.lbSearch.TabIndex = 20;
            this.lbSearch.Text = "Search";
            // 
            // tbSearch
            // 
            this.tbSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tbSearch.Location = new System.Drawing.Point(677, 27);
            this.tbSearch.Name = "tbSearch";
            this.tbSearch.Size = new System.Drawing.Size(154, 20);
            this.tbSearch.TabIndex = 19;
            this.tbSearch.TextChanged += new System.EventHandler(this.tbSearch_TextChanged);
            // 
            // lbStaff
            // 
            this.lbStaff.AutoSize = true;
            this.lbStaff.BackColor = System.Drawing.Color.Transparent;
            this.lbStaff.Font = new System.Drawing.Font("Imprint MT Shadow", 14F);
            this.lbStaff.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lbStaff.Location = new System.Drawing.Point(3, 27);
            this.lbStaff.Name = "lbStaff";
            this.lbStaff.Size = new System.Drawing.Size(48, 23);
            this.lbStaff.TabIndex = 10;
            this.lbStaff.Text = "Staff";
            // 
            // pnlAddStaffSection
            // 
            this.pnlAddStaffSection.Controls.Add(this.pnlAddNewStaff);
            this.pnlAddStaffSection.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlAddStaffSection.Location = new System.Drawing.Point(0, 64);
            this.pnlAddStaffSection.Name = "pnlAddStaffSection";
            this.pnlAddStaffSection.Size = new System.Drawing.Size(837, 159);
            this.pnlAddStaffSection.TabIndex = 4;
            // 
            // pnlAddNewStaff
            // 
            this.pnlAddNewStaff.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pnlAddNewStaff.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlAddNewStaff.Controls.Add(this.mtbCnic);
            this.pnlAddNewStaff.Controls.Add(this.tbAge);
            this.pnlAddNewStaff.Controls.Add(this.lbAge);
            this.pnlAddNewStaff.Controls.Add(this.lbCnic);
            this.pnlAddNewStaff.Controls.Add(this.tbName);
            this.pnlAddNewStaff.Controls.Add(this.cbShift);
            this.pnlAddNewStaff.Controls.Add(this.lbShift);
            this.pnlAddNewStaff.Controls.Add(this.btnAdd);
            this.pnlAddNewStaff.Controls.Add(this.cbStaffCategory);
            this.pnlAddNewStaff.Controls.Add(this.lbName);
            this.pnlAddNewStaff.Controls.Add(this.lbType);
            this.pnlAddNewStaff.Controls.Add(this.lbStaffDetails);
            this.pnlAddNewStaff.Location = new System.Drawing.Point(3, 3);
            this.pnlAddNewStaff.Name = "pnlAddNewStaff";
            this.pnlAddNewStaff.Size = new System.Drawing.Size(831, 156);
            this.pnlAddNewStaff.TabIndex = 33;
            // 
            // mtbCnic
            // 
            this.mtbCnic.Location = new System.Drawing.Point(318, 63);
            this.mtbCnic.Mask = "00000-0000000-0";
            this.mtbCnic.Name = "mtbCnic";
            this.mtbCnic.Size = new System.Drawing.Size(220, 20);
            this.mtbCnic.TabIndex = 45;
            // 
            // tbAge
            // 
            this.tbAge.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tbAge.Location = new System.Drawing.Point(579, 63);
            this.tbAge.Name = "tbAge";
            this.tbAge.Size = new System.Drawing.Size(220, 20);
            this.tbAge.TabIndex = 44;
            // 
            // lbAge
            // 
            this.lbAge.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbAge.AutoSize = true;
            this.lbAge.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbAge.Location = new System.Drawing.Point(576, 43);
            this.lbAge.Name = "lbAge";
            this.lbAge.Size = new System.Drawing.Size(33, 17);
            this.lbAge.TabIndex = 43;
            this.lbAge.Text = "Age";
            // 
            // lbCnic
            // 
            this.lbCnic.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lbCnic.AutoSize = true;
            this.lbCnic.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCnic.Location = new System.Drawing.Point(315, 43);
            this.lbCnic.Name = "lbCnic";
            this.lbCnic.Size = new System.Drawing.Size(39, 17);
            this.lbCnic.TabIndex = 41;
            this.lbCnic.Text = "CNIC";
            // 
            // tbName
            // 
            this.tbName.Location = new System.Drawing.Point(29, 63);
            this.tbName.Name = "tbName";
            this.tbName.Size = new System.Drawing.Size(220, 20);
            this.tbName.TabIndex = 40;
            // 
            // cbShift
            // 
            this.cbShift.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.cbShift.FormattingEnabled = true;
            this.cbShift.Items.AddRange(new object[] {
            "1st Shift",
            "2nd Shift",
            "3rd Shift"});
            this.cbShift.Location = new System.Drawing.Point(318, 114);
            this.cbShift.Name = "cbShift";
            this.cbShift.Size = new System.Drawing.Size(220, 21);
            this.cbShift.TabIndex = 39;
            // 
            // lbShift
            // 
            this.lbShift.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lbShift.AutoSize = true;
            this.lbShift.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbShift.Location = new System.Drawing.Point(315, 94);
            this.lbShift.Name = "lbShift";
            this.lbShift.Size = new System.Drawing.Size(36, 17);
            this.lbShift.TabIndex = 38;
            this.lbShift.Text = "Shift";
            // 
            // btnAdd
            // 
            this.btnAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAdd.BackColor = System.Drawing.SystemColors.ControlLight;
            this.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAdd.Location = new System.Drawing.Point(720, 114);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(102, 30);
            this.btnAdd.TabIndex = 37;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = false;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // cbStaffCategory
            // 
            this.cbStaffCategory.FormattingEnabled = true;
            this.cbStaffCategory.Items.AddRange(new object[] {
            "Ambulance Staff",
            "CTWO",
            "WO",
            "Shift Incharge",
            "Hospital Staff"});
            this.cbStaffCategory.Location = new System.Drawing.Point(29, 114);
            this.cbStaffCategory.Name = "cbStaffCategory";
            this.cbStaffCategory.Size = new System.Drawing.Size(220, 21);
            this.cbStaffCategory.TabIndex = 36;
            // 
            // lbName
            // 
            this.lbName.AutoSize = true;
            this.lbName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbName.Location = new System.Drawing.Point(26, 43);
            this.lbName.Name = "lbName";
            this.lbName.Size = new System.Drawing.Size(45, 17);
            this.lbName.TabIndex = 35;
            this.lbName.Text = "Name";
            // 
            // lbType
            // 
            this.lbType.AutoSize = true;
            this.lbType.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbType.Location = new System.Drawing.Point(26, 94);
            this.lbType.Name = "lbType";
            this.lbType.Size = new System.Drawing.Size(98, 17);
            this.lbType.TabIndex = 34;
            this.lbType.Text = "Staff Category";
            // 
            // lbStaffDetails
            // 
            this.lbStaffDetails.AutoSize = true;
            this.lbStaffDetails.BackColor = System.Drawing.Color.Transparent;
            this.lbStaffDetails.Font = new System.Drawing.Font("Imprint MT Shadow", 14F);
            this.lbStaffDetails.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lbStaffDetails.Location = new System.Drawing.Point(6, 9);
            this.lbStaffDetails.Name = "lbStaffDetails";
            this.lbStaffDetails.Size = new System.Drawing.Size(130, 23);
            this.lbStaffDetails.TabIndex = 33;
            this.lbStaffDetails.Text = "Add New Staff";
            // 
            // pnlHeader
            // 
            this.pnlHeader.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pnlHeader.Controls.Add(this.lbTitleAddStaff);
            this.pnlHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlHeader.ForeColor = System.Drawing.SystemColors.ControlText;
            this.pnlHeader.Location = new System.Drawing.Point(0, 0);
            this.pnlHeader.Name = "pnlHeader";
            this.pnlHeader.Size = new System.Drawing.Size(837, 64);
            this.pnlHeader.TabIndex = 3;
            // 
            // lbTitleAddStaff
            // 
            this.lbTitleAddStaff.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lbTitleAddStaff.AutoSize = true;
            this.lbTitleAddStaff.BackColor = System.Drawing.Color.Transparent;
            this.lbTitleAddStaff.Font = new System.Drawing.Font("Imprint MT Shadow", 20F);
            this.lbTitleAddStaff.ForeColor = System.Drawing.Color.White;
            this.lbTitleAddStaff.Location = new System.Drawing.Point(335, 16);
            this.lbTitleAddStaff.Name = "lbTitleAddStaff";
            this.lbTitleAddStaff.Size = new System.Drawing.Size(180, 32);
            this.lbTitleAddStaff.TabIndex = 4;
            this.lbTitleAddStaff.Text = "ADD STAFF";
            // 
            // StaffID
            // 
            this.StaffID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.StaffID.HeaderText = "Staff ID";
            this.StaffID.Name = "StaffID";
            this.StaffID.ReadOnly = true;
            // 
            // StaffName
            // 
            this.StaffName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.StaffName.HeaderText = "Name";
            this.StaffName.Name = "StaffName";
            this.StaffName.ReadOnly = true;
            // 
            // View
            // 
            this.View.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.View.HeaderText = "View";
            this.View.MinimumWidth = 50;
            this.View.Name = "View";
            this.View.ReadOnly = true;
            this.View.Text = "View";
            this.View.UseColumnTextForButtonValue = true;
            this.View.Width = 50;
            // 
            // Edit
            // 
            this.Edit.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Edit.HeaderText = "Edit";
            this.Edit.MinimumWidth = 50;
            this.Edit.Name = "Edit";
            this.Edit.ReadOnly = true;
            this.Edit.Text = "Edit";
            this.Edit.UseColumnTextForButtonValue = true;
            this.Edit.Width = 50;
            // 
            // Delete
            // 
            this.Delete.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Delete.HeaderText = "Delete";
            this.Delete.MinimumWidth = 50;
            this.Delete.Name = "Delete";
            this.Delete.ReadOnly = true;
            this.Delete.Text = "Delete";
            this.Delete.UseColumnTextForButtonValue = true;
            this.Delete.Width = 50;
            // 
            // ucAddStaff
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pnlStaff);
            this.Controls.Add(this.pnlAddStaffSection);
            this.Controls.Add(this.pnlHeader);
            this.Name = "ucAddStaff";
            this.Size = new System.Drawing.Size(837, 524);
            this.pnlStaff.ResumeLayout(false);
            this.pnlStaff.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtvShiftStaff)).EndInit();
            this.pnlAddStaffSection.ResumeLayout(false);
            this.pnlAddNewStaff.ResumeLayout(false);
            this.pnlAddNewStaff.PerformLayout();
            this.pnlHeader.ResumeLayout(false);
            this.pnlHeader.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlStaff;
        private System.Windows.Forms.Label lbStaff;
        private System.Windows.Forms.Panel pnlAddStaffSection;
        private System.Windows.Forms.Panel pnlHeader;
        private System.Windows.Forms.Label lbTitleAddStaff;
        private System.Windows.Forms.Panel pnlAddNewStaff;
        private System.Windows.Forms.TextBox tbAge;
        private System.Windows.Forms.Label lbAge;
        private System.Windows.Forms.Label lbCnic;
        private System.Windows.Forms.TextBox tbName;
        private System.Windows.Forms.ComboBox cbShift;
        private System.Windows.Forms.Label lbShift;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.ComboBox cbStaffCategory;
        private System.Windows.Forms.Label lbName;
        private System.Windows.Forms.Label lbType;
        private System.Windows.Forms.Label lbStaffDetails;
        private System.Windows.Forms.Label lbSearch;
        private System.Windows.Forms.TextBox tbSearch;
        private System.Windows.Forms.DataGridView dtvShiftStaff;
        private System.Windows.Forms.MaskedTextBox mtbCnic;
        private System.Windows.Forms.DataGridViewTextBoxColumn StaffID;
        private System.Windows.Forms.DataGridViewTextBoxColumn StaffName;
        private System.Windows.Forms.DataGridViewButtonColumn View;
        private System.Windows.Forms.DataGridViewButtonColumn Edit;
        private System.Windows.Forms.DataGridViewButtonColumn Delete;
    }
}
