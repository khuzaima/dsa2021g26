﻿using Ambulance_Service_System.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ambulance_Service_System
{
    public partial class LoginWindow : Form
    {

        private static LoginWindow _instence;
        private Boolean showPassword = false;
        public static LoginWindow Instence
        {
            get
            {
                if (_instence == null)
                {
                    _instence = new LoginWindow();
                }
                return _instence;
            }
        }
        public LoginWindow()
        {
            InitializeComponent();

            lbInvalidTxt.Text = "";
            cData.Instence.load_Ambulances_Data();
            cData.Instence.load_Ambulances_Queue_Data();
            cData.Instence.load_Dispatch_Order_Data();
            cData.Instence.load_FC_Reports_Data();
            cData.Instence.load_Parking_Reports_Data();
            cData.Instence.load_Staff_Data();

        }

        private void rbtnLogin_MouseEnter(object sender, EventArgs e)
        {
            rbtnLogin.BorderSize = 0;
            rbtnLogin.BackColor = Color.FromArgb(0, 70, 230);
            rbtnLogin.Image = Image.FromFile(System.IO.Path.Combine(System.IO.Directory.GetCurrentDirectory(), @"..\..", @"images\login_32px(White).png"));
            rbtnLogin.ForeColor = Color.White;
        }

        private void rbtnLogin_MouseLeave(object sender, EventArgs e)
        {
            rbtnLogin.BorderSize = 2;
            rbtnLogin.BackColor = SystemColors.Control;
            rbtnLogin.Image = Image.FromFile(System.IO.Path.Combine(System.IO.Directory.GetCurrentDirectory(), @"..\..", @"images\login_32px(Blue).png"));
            rbtnLogin.ForeColor = SystemColors.ControlText;
        }

        private void rbtnExit_MouseEnter(object sender, EventArgs e)
        {
            rbtnExit.BorderSize = 0;
            rbtnExit.BackColor = Color.FromArgb(220, 40, 50);
            rbtnExit.Image = Image.FromFile(System.IO.Path.Combine(System.IO.Directory.GetCurrentDirectory(), @"..\..", @"images\shutdown_32px(White).png"));
            rbtnExit.ForeColor = Color.White;
        }

        private void rbtnExit_MouseLeave(object sender, EventArgs e)
        {
            rbtnExit.BorderSize = 2;
            rbtnExit.BackColor = SystemColors.Control;
            rbtnExit.Image = Image.FromFile(System.IO.Path.Combine(System.IO.Directory.GetCurrentDirectory(), @"..\..", @"images\shutdown_32px(Red).png"));
            rbtnExit.ForeColor = SystemColors.ControlText;
        }

        private void txtPasswordField_Enter(object sender, EventArgs e)
        {
            if (txtPasswordField.Text == "Password")
            {
                txtPasswordField.Text = "";
                txtPasswordField.ForeColor = SystemColors.ControlText;
                txtPasswordField.UseSystemPasswordChar = true;
            }
        }

        private void txtPasswordField_Leave(object sender, EventArgs e)
        {
            if (txtPasswordField.Text == "")
            {
                txtPasswordField.Text = "Password";
                txtPasswordField.ForeColor = SystemColors.ControlDark;
                txtPasswordField.UseSystemPasswordChar = false;
            }
        }

        private void txtEmailField_Enter(object sender, EventArgs e)
        {
            if (txtEmailField.Text == "Username")
            {
                txtEmailField.Text = "";
                txtEmailField.ForeColor = SystemColors.ControlText;
            }
        }

        private void txtEmailField_Leave(object sender, EventArgs e)
        {
            if (txtEmailField.Text == "")
            {
                txtEmailField.Text = "Username";
                txtEmailField.ForeColor = SystemColors.ControlDark;
            }
        }

        private void txtEmailField_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send("{TAB}");
            }
            else if (!lbInvalidTxt.Equals(""))
            {
                lbInvalidTxt.Text = "";
            }
        }

        private void txtPasswordField_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (txtEmailField.Text.Equals("Username"))
                {
                    SendKeys.Send("+{TAB}");
                }
                else
                {
                    Login();
                }
            }
            else if (!lbInvalidTxt.Equals(""))
            {
                lbInvalidTxt.Text = "";
            }
        }

        private void rbtnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Login()
        {
            bool flag = true;
            if (txtEmailField.Text.Equals("admin") && txtPasswordField.Text.Equals("admin"))
            {
                this.Hide();
                MainWindow.Instence.Show();
            }
            else
            {
                foreach (var item in cData.list_of_staff)
                {
                    if (item.email.Equals(txtEmailField.Text) && item.password.Equals(txtPasswordField.Text))
                    {
                        flag = false;
                        this.Hide();
                        MainWindow.Instence.Show();
                    }
                }
                if(flag == true)
                {
                    lbInvalidTxt.Text = "Invalid Credentials";
                }
            }
        }

        private void rbtnLogin_Click(object sender, EventArgs e)
        {
            Login();
        }

        private void btnHidePassword_Click(object sender, EventArgs e)
        {
            if(showPassword == false)
            {
                btnHidePassword.Image = Image.FromFile(System.IO.Path.Combine(System.IO.Directory.GetCurrentDirectory(), @"..\..", @"images\hide_22px.png"));
                if (txtPasswordField.Text != "Password")
                {
                    txtPasswordField.UseSystemPasswordChar = false;
                }
                showPassword = true;
            }
            else
            {
                btnHidePassword.Image = Image.FromFile(System.IO.Path.Combine(System.IO.Directory.GetCurrentDirectory(), @"..\..", @"images\eye_22px.png"));
                if (txtPasswordField.Text != "Password")
                {
                    txtPasswordField.UseSystemPasswordChar = true;
                }
                showPassword = false;
            }
        }

        private void LoginWindow_VisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible)
            {
                txtEmailField.ResetText();
                txtEmailField.ForeColor = SystemColors.ControlText;
                txtPasswordField.Text = "Password";
                txtPasswordField.ForeColor = SystemColors.ControlDark;
                txtPasswordField.UseSystemPasswordChar = false;
                txtEmailField.Select();

            }
        }
    }
}
