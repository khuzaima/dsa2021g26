﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ambulance_Service_System
{
    public partial class ucAmbulanceStatus : UserControl
    {

        private static ucAmbulanceStatus _instence;
        public static ucAmbulanceStatus Instence
        {
            get
            {
                if(_instence == null)
                {
                    _instence = new ucAmbulanceStatus();
                }
                return _instence;
            }
        }
        public ucAmbulanceStatus()
        {
            InitializeComponent();
            LoadDataInDtv();
        }
        public void RefreshUC()
        { 
            LoadDataInDtv();
        }

        private void LoadDataInDtv()
        {
            dataGridView1.Rows.Clear();
            for (int i = 0; i < cData.list_of_ambulances.Count; i++)
            {
                dataGridView1.Rows.Add(cData.list_of_ambulances[i].ID, cData.list_of_ambulances[i].plateNum, cData.list_of_ambulances[i].status);
            }
        }
    }
}
