﻿
namespace Ambulance_Service_System
{
    partial class ucParkingLog
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlHeader = new System.Windows.Forms.Panel();
            this.lbTitleLog = new System.Windows.Forms.Label();
            this.pnlBody = new System.Windows.Forms.Panel();
            this.lbSearch = new System.Windows.Forms.Label();
            this.tbSearch = new System.Windows.Forms.TextBox();
            this.lbLog = new System.Windows.Forms.Label();
            this.dtvParkingLog = new System.Windows.Forms.DataGridView();
            this.AmbulanceID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AmbulancePlateNum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DateAndTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Status = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlHeader.SuspendLayout();
            this.pnlBody.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtvParkingLog)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlHeader
            // 
            this.pnlHeader.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pnlHeader.Controls.Add(this.lbTitleLog);
            this.pnlHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlHeader.ForeColor = System.Drawing.SystemColors.ControlText;
            this.pnlHeader.Location = new System.Drawing.Point(0, 0);
            this.pnlHeader.Name = "pnlHeader";
            this.pnlHeader.Size = new System.Drawing.Size(837, 64);
            this.pnlHeader.TabIndex = 2;
            // 
            // lbTitleLog
            // 
            this.lbTitleLog.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lbTitleLog.AutoSize = true;
            this.lbTitleLog.BackColor = System.Drawing.Color.Transparent;
            this.lbTitleLog.Font = new System.Drawing.Font("Imprint MT Shadow", 20F);
            this.lbTitleLog.ForeColor = System.Drawing.Color.White;
            this.lbTitleLog.Location = new System.Drawing.Point(297, 16);
            this.lbTitleLog.Name = "lbTitleLog";
            this.lbTitleLog.Size = new System.Drawing.Size(220, 32);
            this.lbTitleLog.TabIndex = 4;
            this.lbTitleLog.Text = "PARKING LOG";
            // 
            // pnlBody
            // 
            this.pnlBody.Controls.Add(this.lbSearch);
            this.pnlBody.Controls.Add(this.tbSearch);
            this.pnlBody.Controls.Add(this.lbLog);
            this.pnlBody.Controls.Add(this.dtvParkingLog);
            this.pnlBody.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlBody.Location = new System.Drawing.Point(0, 64);
            this.pnlBody.Name = "pnlBody";
            this.pnlBody.Size = new System.Drawing.Size(837, 460);
            this.pnlBody.TabIndex = 7;
            // 
            // lbSearch
            // 
            this.lbSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbSearch.AutoSize = true;
            this.lbSearch.Location = new System.Drawing.Point(629, 24);
            this.lbSearch.Name = "lbSearch";
            this.lbSearch.Size = new System.Drawing.Size(41, 13);
            this.lbSearch.TabIndex = 12;
            this.lbSearch.Text = "Search";
            // 
            // tbSearch
            // 
            this.tbSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tbSearch.Location = new System.Drawing.Point(672, 21);
            this.tbSearch.Name = "tbSearch";
            this.tbSearch.Size = new System.Drawing.Size(154, 20);
            this.tbSearch.TabIndex = 11;
            this.tbSearch.TextChanged += new System.EventHandler(this.tbSearch_TextChanged);
            // 
            // lbLog
            // 
            this.lbLog.AutoSize = true;
            this.lbLog.BackColor = System.Drawing.Color.Transparent;
            this.lbLog.Font = new System.Drawing.Font("Imprint MT Shadow", 14F);
            this.lbLog.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lbLog.Location = new System.Drawing.Point(9, 21);
            this.lbLog.Name = "lbLog";
            this.lbLog.Size = new System.Drawing.Size(113, 23);
            this.lbLog.TabIndex = 10;
            this.lbLog.Text = "Parking Log";
            // 
            // dtvParkingLog
            // 
            this.dtvParkingLog.AllowUserToAddRows = false;
            this.dtvParkingLog.AllowUserToDeleteRows = false;
            this.dtvParkingLog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtvParkingLog.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dtvParkingLog.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtvParkingLog.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.AmbulanceID,
            this.AmbulancePlateNum,
            this.DateAndTime,
            this.Status});
            this.dtvParkingLog.Location = new System.Drawing.Point(11, 47);
            this.dtvParkingLog.Name = "dtvParkingLog";
            this.dtvParkingLog.ReadOnly = true;
            this.dtvParkingLog.Size = new System.Drawing.Size(815, 398);
            this.dtvParkingLog.TabIndex = 9;
            // 
            // AmbulanceID
            // 
            this.AmbulanceID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.AmbulanceID.HeaderText = "Ambulance ID";
            this.AmbulanceID.Name = "AmbulanceID";
            this.AmbulanceID.ReadOnly = true;
            // 
            // AmbulancePlateNum
            // 
            this.AmbulancePlateNum.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.AmbulancePlateNum.HeaderText = "Ambulance Plate No.";
            this.AmbulancePlateNum.Name = "AmbulancePlateNum";
            this.AmbulancePlateNum.ReadOnly = true;
            // 
            // DateAndTime
            // 
            this.DateAndTime.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.DateAndTime.HeaderText = "Date & Time";
            this.DateAndTime.Name = "DateAndTime";
            this.DateAndTime.ReadOnly = true;
            // 
            // Status
            // 
            this.Status.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Status.HeaderText = "Status";
            this.Status.Name = "Status";
            this.Status.ReadOnly = true;
            // 
            // ucParkingLog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pnlBody);
            this.Controls.Add(this.pnlHeader);
            this.Name = "ucParkingLog";
            this.Size = new System.Drawing.Size(837, 524);
            this.pnlHeader.ResumeLayout(false);
            this.pnlHeader.PerformLayout();
            this.pnlBody.ResumeLayout(false);
            this.pnlBody.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtvParkingLog)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlHeader;
        private System.Windows.Forms.Label lbTitleLog;
        private System.Windows.Forms.Panel pnlBody;
        private System.Windows.Forms.Label lbSearch;
        private System.Windows.Forms.TextBox tbSearch;
        private System.Windows.Forms.Label lbLog;
        private System.Windows.Forms.DataGridView dtvParkingLog;
        private System.Windows.Forms.DataGridViewTextBoxColumn AmbulanceID;
        private System.Windows.Forms.DataGridViewTextBoxColumn AmbulancePlateNum;
        private System.Windows.Forms.DataGridViewTextBoxColumn DateAndTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn Status;
    }
}
