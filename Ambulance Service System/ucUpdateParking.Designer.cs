﻿
namespace Ambulance_Service_System
{
    partial class ucUpdateParking
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlHeader = new System.Windows.Forms.Panel();
            this.lbTitleUpdate = new System.Windows.Forms.Label();
            this.pnlBody = new System.Windows.Forms.Panel();
            this.lbSearch = new System.Windows.Forms.Label();
            this.tbSearch = new System.Windows.Forms.TextBox();
            this.lbAmbulances = new System.Windows.Forms.Label();
            this.dtvAmbulances = new System.Windows.Forms.DataGridView();
            this.AmbulanceID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AmbulancePlateNum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Unpark = new System.Windows.Forms.DataGridViewButtonColumn();
            this.Park = new System.Windows.Forms.DataGridViewButtonColumn();
            this.pnlHeader.SuspendLayout();
            this.pnlBody.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtvAmbulances)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlHeader
            // 
            this.pnlHeader.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pnlHeader.Controls.Add(this.lbTitleUpdate);
            this.pnlHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlHeader.ForeColor = System.Drawing.SystemColors.ControlText;
            this.pnlHeader.Location = new System.Drawing.Point(0, 0);
            this.pnlHeader.Name = "pnlHeader";
            this.pnlHeader.Size = new System.Drawing.Size(837, 64);
            this.pnlHeader.TabIndex = 1;
            // 
            // lbTitleUpdate
            // 
            this.lbTitleUpdate.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lbTitleUpdate.AutoSize = true;
            this.lbTitleUpdate.BackColor = System.Drawing.Color.Transparent;
            this.lbTitleUpdate.Font = new System.Drawing.Font("Imprint MT Shadow", 20F);
            this.lbTitleUpdate.ForeColor = System.Drawing.Color.White;
            this.lbTitleUpdate.Location = new System.Drawing.Point(279, 16);
            this.lbTitleUpdate.Name = "lbTitleUpdate";
            this.lbTitleUpdate.Size = new System.Drawing.Size(281, 32);
            this.lbTitleUpdate.TabIndex = 4;
            this.lbTitleUpdate.Text = "UPDATE PARKING";
            // 
            // pnlBody
            // 
            this.pnlBody.Controls.Add(this.lbSearch);
            this.pnlBody.Controls.Add(this.tbSearch);
            this.pnlBody.Controls.Add(this.lbAmbulances);
            this.pnlBody.Controls.Add(this.dtvAmbulances);
            this.pnlBody.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlBody.Location = new System.Drawing.Point(0, 64);
            this.pnlBody.Name = "pnlBody";
            this.pnlBody.Size = new System.Drawing.Size(837, 460);
            this.pnlBody.TabIndex = 6;
            // 
            // lbSearch
            // 
            this.lbSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbSearch.AutoSize = true;
            this.lbSearch.Location = new System.Drawing.Point(629, 24);
            this.lbSearch.Name = "lbSearch";
            this.lbSearch.Size = new System.Drawing.Size(41, 13);
            this.lbSearch.TabIndex = 12;
            this.lbSearch.Text = "Search";
            // 
            // tbSearch
            // 
            this.tbSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tbSearch.Location = new System.Drawing.Point(672, 21);
            this.tbSearch.Name = "tbSearch";
            this.tbSearch.Size = new System.Drawing.Size(154, 20);
            this.tbSearch.TabIndex = 11;
            this.tbSearch.TextChanged += new System.EventHandler(this.tbSearch_TextChanged);
            // 
            // lbAmbulances
            // 
            this.lbAmbulances.AutoSize = true;
            this.lbAmbulances.BackColor = System.Drawing.Color.Transparent;
            this.lbAmbulances.Font = new System.Drawing.Font("Imprint MT Shadow", 14F);
            this.lbAmbulances.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lbAmbulances.Location = new System.Drawing.Point(9, 21);
            this.lbAmbulances.Name = "lbAmbulances";
            this.lbAmbulances.Size = new System.Drawing.Size(112, 23);
            this.lbAmbulances.TabIndex = 10;
            this.lbAmbulances.Text = "Ambulances";
            // 
            // dtvAmbulances
            // 
            this.dtvAmbulances.AllowUserToAddRows = false;
            this.dtvAmbulances.AllowUserToDeleteRows = false;
            this.dtvAmbulances.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtvAmbulances.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dtvAmbulances.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtvAmbulances.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.AmbulanceID,
            this.AmbulancePlateNum,
            this.Unpark,
            this.Park});
            this.dtvAmbulances.Location = new System.Drawing.Point(11, 47);
            this.dtvAmbulances.Name = "dtvAmbulances";
            this.dtvAmbulances.ReadOnly = true;
            this.dtvAmbulances.Size = new System.Drawing.Size(815, 398);
            this.dtvAmbulances.TabIndex = 9;
            this.dtvAmbulances.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtvAmbulances_CellContentClick);
            // 
            // AmbulanceID
            // 
            this.AmbulanceID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.AmbulanceID.HeaderText = "Ambulance ID";
            this.AmbulanceID.Name = "AmbulanceID";
            this.AmbulanceID.ReadOnly = true;
            // 
            // AmbulancePlateNum
            // 
            this.AmbulancePlateNum.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.AmbulancePlateNum.HeaderText = "Ambulance Plate No.";
            this.AmbulancePlateNum.Name = "AmbulancePlateNum";
            this.AmbulancePlateNum.ReadOnly = true;
            // 
            // Unpark
            // 
            this.Unpark.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Unpark.HeaderText = "Unpark";
            this.Unpark.MinimumWidth = 50;
            this.Unpark.Name = "Unpark";
            this.Unpark.ReadOnly = true;
            this.Unpark.Text = "Unpark";
            this.Unpark.UseColumnTextForButtonValue = true;
            this.Unpark.Width = 50;
            // 
            // Park
            // 
            this.Park.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Park.HeaderText = "Park";
            this.Park.MinimumWidth = 50;
            this.Park.Name = "Park";
            this.Park.ReadOnly = true;
            this.Park.Text = "Park";
            this.Park.UseColumnTextForButtonValue = true;
            this.Park.Width = 50;
            // 
            // ucUpdateParking
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pnlBody);
            this.Controls.Add(this.pnlHeader);
            this.Name = "ucUpdateParking";
            this.Size = new System.Drawing.Size(837, 524);
            this.pnlHeader.ResumeLayout(false);
            this.pnlHeader.PerformLayout();
            this.pnlBody.ResumeLayout(false);
            this.pnlBody.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtvAmbulances)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlHeader;
        private System.Windows.Forms.Label lbTitleUpdate;
        private System.Windows.Forms.Panel pnlBody;
        private System.Windows.Forms.Label lbSearch;
        private System.Windows.Forms.TextBox tbSearch;
        private System.Windows.Forms.Label lbAmbulances;
        private System.Windows.Forms.DataGridView dtvAmbulances;
        private System.Windows.Forms.DataGridViewTextBoxColumn AmbulanceID;
        private System.Windows.Forms.DataGridViewTextBoxColumn AmbulancePlateNum;
        private System.Windows.Forms.DataGridViewButtonColumn Unpark;
        private System.Windows.Forms.DataGridViewButtonColumn Park;
    }
}
