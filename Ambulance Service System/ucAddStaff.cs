﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ambulance_Service_System
{
    public partial class ucAddStaff : UserControl
    {
        private static ucAddStaff _instence;

        private readonly Random _random = new Random();
        public int Update_index { get; set; }

        public static ucAddStaff Instence
        {
            get
            {
                if (_instence == null)
                {
                    _instence = new ucAddStaff();
                }
                return _instence;
            }
        }
        public ucAddStaff()
        {
            InitializeComponent();
            LoadDataInDtv();
        }

        private void clearFields()
        {
            tbSearch.Text = "";
            tbName.Text = "";
            tbAge.Text = "";
            mtbCnic.Text = "";
            cbShift.Text = "";
            cbStaffCategory.Text = "";
        }

        public void RefreshUC()
        {
            clearFields();
            tbSearch.Text = "";
            btnAdd.Text = "Add";
            LoadDataInDtv();
        }

        private void LoadDataInDtv()
        {
            dtvShiftStaff.Rows.Clear();
            for (int i = 0; i < cData.list_of_staff.Count; i++)
            {
                dtvShiftStaff.Rows.Add(cData.list_of_staff[i].id, cData.list_of_staff[i].name);
            }
        }

        private String GenerateStaffId()
        {
            String id = "";
            bool flag = true;
            for (int i = 1; i <= cData.list_of_staff.Count + 1; i++)
            {
                id = "STF-" + i;
                for (int j = 0; j < cData.list_of_staff.Count; j++)
                {
                    if (id.Equals(cData.list_of_staff[j].id))
                    {
                        flag = false;
                    }
                }
                if (flag == false)
                {
                    flag = true;
                }
                else
                {
                    return id;
                }
            }
            return id;
        }
        private String GenerateStaffEmail(string id)
        {
            return ($"{id}@service.pk");
        }
        private string RandomString(int size, bool lowerCase = false)
        {
            var builder = new StringBuilder(size);
            char offset = lowerCase ? 'a' : 'A';
            const int lettersOffset = 26;

            for (var i = 0; i < size; i++)
            {
                var @char = (char)_random.Next(offset, offset + lettersOffset);
                builder.Append(@char);
            }
            return lowerCase ? builder.ToString().ToLower() : builder.ToString();
        }
        private String GenerateRandomPassword(int size)
        {
            bool flag;
            for (int i = 0; i < cData.list_of_staff.Count; i++)
            {
                flag = true;
                string pass = RandomString(size, true);
                foreach (var item in cData.list_of_staff)
                {
                    if (item.password == pass)
                    {
                        flag = false;
                        break;
                    }
                    if (flag)
                    {
                        return pass;
                    }
                }
            }
            return null;
        }


        private void btnAdd_Click(object sender, EventArgs e) 
        {
            if (btnAdd.Text == "Add")
            {
                if (tbName.Text != "" && tbAge.Text != "" && mtbCnic.MaskFull && cbShift.Text != "" && cbStaffCategory.Text != "")
                {
                    string id = GenerateStaffId();

                    cData.list_of_staff.Add(new cStaff(tbName.Text, mtbCnic.Text, Convert.ToInt32(tbAge.Text), cbStaffCategory.Text, cbShift.Text, id, GenerateStaffEmail(id), GenerateRandomPassword(8)));
                    clearFields();
                    LoadDataInDtv();
                }
                else
                {
                    MessageBox.Show("Invalid Data.");
                }
            }
            else
            {
                if (tbName.Text != "" && tbAge.Text != "" && mtbCnic.MaskFull && cbShift.Text != "" && cbStaffCategory.Text != "")
                {
                    cData.list_of_staff[Update_index].name = tbName.Text;
                    cData.list_of_staff[Update_index].cnic = mtbCnic.Text;
                    cData.list_of_staff[Update_index].age = Convert.ToInt32(tbAge.Text);
                    cData.list_of_staff[Update_index].shift = cbShift.Text;
                    cData.list_of_staff[Update_index].category = cbStaffCategory.Text;
                    MessageBox.Show("Data Updated Successfully.");
                    RefreshUC();
                }
                else
                {
                    MessageBox.Show("Invalid Data.");
                }
            }
        }

        private void dtvShiftStaff_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex>=0)
            {
                int index = getObjectIndex(dtvShiftStaff.Rows[e.RowIndex].Cells[0].Value.ToString());
                if (e.ColumnIndex == 2)
                {
                    MessageBox.Show($"Staff id : {cData.list_of_staff[index].id}\n" +
                        $"Staff name : {cData.list_of_staff[index].name}\n" +
                        $"Staff cnic : {cData.list_of_staff[index].cnic}\n" +
                        $"Staff age : {cData.list_of_staff[index].age}\n" +
                        $"Staff category : {cData.list_of_staff[index].category}\n" +
                        $"Staff shift : {cData.list_of_staff[index].shift}\n" +
                        $"Staff email : {cData.list_of_staff[index].email}\n" +
                        $"Staff password : {cData.list_of_staff[index].password}\n");
                }
                if (e.ColumnIndex == 3)
                {
                    cStaff staff = cData.list_of_staff[index];
                    tbName.Text = staff.name;
                    tbAge.Text = staff.age.ToString();
                    mtbCnic.Text = staff.cnic;
                    cbShift.Text = staff.shift;
                    cbStaffCategory.Text = staff.category;
                    btnAdd.Text = "Update";
                    Update_index = index;
                }
                if (e.ColumnIndex == 4)
                {
                    cData.list_of_staff.RemoveAt(index);
                }
                LoadDataInDtv();
            }
        }

        public int getObjectIndex(string id)
        {
            for (int i = 0; i < cData.list_of_staff.Count; i++)
            {
                if (cData.list_of_staff[i].id == id)
                {
                    return i;
                }
            }
            return -1;
        }

        private void tbSearch_TextChanged(object sender, EventArgs e)
        {
            dtvShiftStaff.Rows.Clear();
            foreach (var item in cData.list_of_staff)
            {
                if (item.id.Contains(tbSearch.Text) || item.cnic.Contains(tbSearch.Text))
                {
                    dtvShiftStaff.Rows.Add(item.id, item.name);
                }
            }
        }
    }
}
