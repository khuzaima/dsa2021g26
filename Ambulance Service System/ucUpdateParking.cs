﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ambulance_Service_System
{
    public partial class ucUpdateParking : UserControl
    {

        private static ucUpdateParking _instence;
        public static ucUpdateParking Instence
        {
            get
            {
                if(_instence == null)
                {
                    _instence = new ucUpdateParking();
                }
                return _instence;
            }
        }
        public ucUpdateParking()
        {
            InitializeComponent();
            LoadDataInDtv();
        }
        public void RefreshUC()
        {
            tbSearch.Text = "";
            LoadDataInDtv();
        }

        private void LoadDataInDtv()
        {
            dtvAmbulances.Rows.Clear();
            for (int i = 0; i < cData.list_of_ambulances.Count; i++)
            {
                dtvAmbulances.Rows.Add(cData.list_of_ambulances[i].ID, cData.list_of_ambulances[i].plateNum);
            }
        }

        public int getObjectIndex(string id)
        {
            for (int i = 0; i < cData.list_of_ambulances.Count; i++)
            {
                if (cData.list_of_ambulances[i].ID == id)
                {
                    return i;
                }
            }
            return -1;
        }

        private void dtvAmbulances_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                int index = getObjectIndex(dtvAmbulances.Rows[e.RowIndex].Cells[0].Value.ToString());
                if (e.ColumnIndex == 2)
                {
                    cData.list_of_parking_reports.Add(new cParkingReport(cData.list_of_ambulances[index].ID,
                        cData.list_of_ambulances[index].plateNum, DateTime.Now, "Unparked"));
                }
                if (e.ColumnIndex == 3)
                {
                    cData.list_of_parking_reports.Add(new cParkingReport(cData.list_of_ambulances[index].ID,
                        cData.list_of_ambulances[index].plateNum, DateTime.Now, "Parked"));
                }
            }
        }

        private void tbSearch_TextChanged(object sender, EventArgs e)
        {
            dtvAmbulances.Rows.Clear();
            foreach (var item in cData.list_of_ambulances)
            {
                if (item.ID.Contains(tbSearch.Text) || item.plateNum.Contains(tbSearch.Text))
                {
                    dtvAmbulances.Rows.Add(item.ID, item.plateNum);
                }
            }
        }
    }
}
