﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ambulance_Service_System
{
    public partial class MainWindow : Form
    {

        private static MainWindow _instence;
        public static MainWindow Instence
        {
            get
            {
                if (_instence == null)
                {
                    _instence = new MainWindow();
                }
                return _instence;
            }
        }
        public static Panel main_Panel;

        public MainWindow()
        {
            InitializeComponent();
            customizeDesign();
            panel.Controls.Add(ucHome.Instence);
            ucHome.Instence.Dock = DockStyle.Fill;
            ucHome.Instence.BringToFront();
            main_Panel = panel;
        }

        private void customizeDesign()
        {
            pnlDispatchSubMenu.Visible = false;
            pnlParkingSubMenu.Visible = false;
        }

        private void hideSubMenu()
        {
            if (pnlDispatchSubMenu.Visible == true)
            {
                pnlDispatchSubMenu.Visible = false;
            }
            if (pnlParkingSubMenu.Visible == true)
            {
                pnlParkingSubMenu.Visible = false;
            }
        }

        private void showSubMenu(Panel subMenu)
        {
            if (subMenu.Visible == false)
            {
                hideSubMenu();
                subMenu.Visible = true;
            }
            else
            {
                subMenu.Visible = false;
            }
        }

        private void btnHome_Click(object sender, EventArgs e)
        {
            hideSubMenu();
            pnlSelectedBtn.Height = btnHome.Height;
            pnlSelectedBtn.Top = btnHome.Top;
            ChangeUC.Home();
        }

        private void btnDispatchAmbulances_Click(object sender, EventArgs e)
        {
            showSubMenu(pnlDispatchSubMenu);
            pnlSelectedBtn.Height = btnDispatchAmbulances.Height;
            pnlSelectedBtn.Top = btnDispatchAmbulances.Top;
        }

        private void btnDispatch_Click(object sender, EventArgs e)
        {
            ChangeUC.DispatchAmbulace();
        }

        private void btnAmbulancesStatus_Click(object sender, EventArgs e)
        {
            ChangeUC.AmbulanceStatus();
        }

        private void btnPt_FC_Report_Click(object sender, EventArgs e)
        {
            hideSubMenu();
            pnlSelectedBtn.Height = btnPt_FC_Report.Height;
            pnlSelectedBtn.Top = btnPt_FC_Report.Top;
            ChangeUC.Patient_FC_Report();
        }

        private void btnParkingManagement_Click(object sender, EventArgs e)
        {
            showSubMenu(pnlParkingSubMenu);
            pnlSelectedBtn.Height = btnParkingManagement.Height;
            pnlSelectedBtn.Top = btnParkingManagement.Top;
        }

        private void btnRegisterdAmbulances_Click(object sender, EventArgs e)
        {
            ChangeUC.RegisterAmbulance();
        }

        private void btnUpdateParking_Click(object sender, EventArgs e)
        {
            ChangeUC.UpdateParking();
        }

        private void btnParkingLog_Click(object sender, EventArgs e)
        {
            ChangeUC.ParkingLog();
        }

        private void btnShiftChange_Click(object sender, EventArgs e)
        {
            hideSubMenu();
            pnlSelectedBtn.Height = btnShiftChange.Height;
            pnlSelectedBtn.Top = btnShiftChange.Top;
            ChangeUC.StaffShifts();
        }

        private void btnAddStaff_Click(object sender, EventArgs e)
        {
            hideSubMenu();
            pnlSelectedBtn.Height = btnAddStaff.Height;
            pnlSelectedBtn.Top = btnAddStaff.Top;
            ChangeUC.AddStaff();
        }

        private void btnHospitals_Click(object sender, EventArgs e)
        {
            hideSubMenu();
            pnlSelectedBtn.Height = btnHospitals.Height;
            pnlSelectedBtn.Top = btnHospitals.Top;
            ChangeUC.Hospitals();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnLogout_Click(object sender, EventArgs e)
        {
            contextMenuStrip1.Show(btnLogout, 0, btnLogout.Height);
        }

        private void MainWindow_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void logoutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            LoginWindow.Instence.Show();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void MainWindow_FormClosing(object sender, FormClosingEventArgs e)
        {
            cData.Instence.Save_Ambulances_Data();
            cData.Instence.Save_Ambulances_Queue_Data();
            cData.Instence.save_Dispatch_Order_Data();
            cData.Instence.save_FC_Reports_Data();
            cData.Instence.save_Parking_Reports_Data();
            cData.Instence.save_Staff_Data();
        }
    }
}
